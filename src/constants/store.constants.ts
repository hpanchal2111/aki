export namespace StoreConstants {
  export const DEVICES_KEY:string = 'devices';
  export const DEVICE_DATA_KEY:string = 'deviceData';
  //export const DEVICE_INFO_KEY:string = 'deviceInfo';
  export const MACHINE_LEANING_SESSION_ID_KEY:string = 'machineLearningSessionId';
}
