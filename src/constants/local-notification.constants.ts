import { ILocalNotification } from "@ionic-native/local-notifications";

export namespace LocalNotificationConstants {
  export const DEFAULT_TITLE:string = 'LOCAL_NOTIFICATION_DEFAULT_TITLE';
  export const DEFAULT_OPTIONS:ILocalNotification = {

  };
  export const SEND_WHILE_IN_FOREGROUND:boolean = false;
}
