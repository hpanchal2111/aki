export namespace AlertConstants {
  export const MAX: number = 1000;
  export const TEST_WALK_STEP: number = 5;
  export const SLOUCHING_PROMPT: string = "SLOUCHING_PROMPT";
  export const CROSS_LEGS_PROMPT: string = "CROSS_LEGS_PROMPT";
  export const TRACK_POSTURE_PROMPT: Array<string> = ["SLOUCHING_PROMPT", "CROSS_LEGS_PROMPT"];
  export const ALERT_MESSAGE_NO_TOAST: Array<string> = ["WALKING_STEP_TAKEN"];
  export const ALERT_MESSAGE_NO_LOCAL: Array<string> = ["WALKING_STEP_TAKEN"];

  // List of pages that will exit application on press of back button
  export const EXIT_APP_ON_BACK_OF_FF_PAGES: Array<string> = ["LoginPage", "SignupPage"];
}
