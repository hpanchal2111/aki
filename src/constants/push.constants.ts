import { Priority, PushOptions } from "@ionic-native/push";

export namespace PushConstants {
  export const DEFAULT_CHANNEL_NAME:string = 'PUSH_CHANNEL';
  export const DEFAULT_CHANNEL_DESCRIPTION:string = 'PUSH_DESCRIPTION';
  export const DEFAULT_CHANNEL_IMPORTANCE:Priority = 3;
  export const OPTIONS:PushOptions = {
    ios: {
      alert: true,
      badge: true,
      sound: true,
      clearBadge: true,
    },
    android: {
      sound: true,
      clearBadge: true,
    },
  }
}
