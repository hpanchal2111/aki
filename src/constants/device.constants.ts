import { AssignBytesType } from "../types/assign-bytes.type";
import { LocationType } from "../types/location.type";

export interface AppToDeviceCommandsInterface {
  ddGet: string;
  sensorLogFrequency: string,
  sensorAssignment: string,
  streamStart: string,
  streamStop: string,
  sensorCheck: string,
}
export interface DeviceToAppCommandsInterface {
  deviceDetails: string;
  sensorLog: string;
  sensorLogFrequency: string;
  sensorAssignment: string;
}
export namespace DeviceConstants {
  export const DEVICE_NAMES: Array<string> = ['BM71_BLE', 'RN_BLE'];
  export const WRITE_CHAR_UUID: string = '49535343-1E4D-4BD9-BA61-23C647249616';
  export const WRITE_SERVICE_UUID: string = '49535343-FE7D-4AE5-8FA9-9FAFD205E455';
  export const NOTIFY_CHAR_UUID: string = '49535343-1E4D-4BD9-BA61-23C647249616';
  export const NOTIFY_SERVICE_UUID: string = '49535343-FE7D-4AE5-8FA9-9FAFD205E455';
  export const READ_CHAR_UUID: string = null;
  export const READ_SERVICE_UUID: string = null;
  export const APPEND_TO_DATA:string = '&&&';
  export const SEPARATOR:string = '>';
  export const SUB_SEPARATOR:string = ',';
  export const ASSIGN_BYTES_DEFAULT:AssignBytesType = '111111';
  export const SENSORY_LOG_FREQUENCY_DEFAULT:number = 10;
  export const DEFAULT_LOCATION:LocationType = 'belt';
  export const QUEUE_COUNT:number = 10000;
  export const SYNC_STREAM_TIME:number = 2000;
  export const MAX_RETRIES:number = 20;
  export const SAVE_DEVICE_INFO_TIME:number = 20000;
  export const RSSI_THRESHOLD_DISCONNECT:number = -100;
  export const RSSI_THRESHOLD_PAIR:number = -70;
  export const RSSI_TIMEOUT:number = 2000;
  export const RSSI_NO_SIGNAL:number = -100;
  export const TIME_TO_PAIR:number = 5000;
  export const DEFAULT_DATA_TO_CAPTURE:Array<string> = [
    'ACC_X',
    'ACC_Y',
    'ACC_Z',
    'GYR_X',
    'GYR_Y',
    'GYR_Z'
  ];
  export const TRIM_END_OF_DATA_REG_EXP: RegExp = /&+$/;
  export const FIX_DD_REG_EX_FIND: RegExp = /^DD{/;
  export const FIX_DD_REG_EX_REPLACE: string = 'DD>{';
  export const START_OF_COMMAND_REG_EXP: RegExp = /^[/w]+[>{]/;
  export const COMMANDS_TO_DEVICE: AppToDeviceCommandsInterface = {
    ddGet: 'DD>GET',
    sensorLogFrequency: 'SLF>SET>',
    sensorAssignment: 'SA>SET>',
    streamStart: 'STRT',
    streamStop: 'STOP',
    sensorCheck: 'BLINK',
  };
  export const COMMANDS_FROM_DEVICE: DeviceToAppCommandsInterface = {
    deviceDetails: 'DD',
    sensorLog: 'SL',
    sensorLogFrequency: 'SLF',
    sensorAssignment: 'SA'
  };

  // max storage length for device data
  export const MAX_NGRX_DEVICE_STORE_DATA = 1000;
  export const PRESS_BACK_TO_EXIT_TIMEOUT:number = 5000;
}
