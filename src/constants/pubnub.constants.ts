/**
 * Constants used for PubNubProvider
*/
import { ENV } from "../../.env";
import { PubNubSubscribeArgs } from "../providers/pub-nub/pub-nub";

export namespace PubNubConstants {
    export const PUBLISH_KEY: string = ENV.PUBNUB_PUBLISH_KEY;
    export const SUBSCRIBE_KEY: string = ENV.PUBNUB_SUBSCRIBE_KEY;
    export const ML_CHANNEL_PREFIX: string = 'machine.learning.user.';
    export const TYPE_ENDS_WITH_REGEX:RegExp = /RTW$/;
    export const CHANNEL_DEFAULTS:PubNubSubscribeArgs = {
      triggerEvents: true,
      withPresence: true
    }
}
