export namespace DashboardConstants {
  export const DASHBOARD:string = "DashboardPage";
  export const DASHBOARD_INDEX:number = 2;
  export const DASHBOARD_REDIRECT_TIMEOUT:number = 5000;
}
