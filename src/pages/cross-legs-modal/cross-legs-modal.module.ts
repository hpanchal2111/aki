import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CrossLegsModalPage } from './cross-legs-modal';

@NgModule({
  declarations: [
    CrossLegsModalPage,
  ],
  imports: [
    IonicPageModule.forChild(CrossLegsModalPage),
  ],
})
export class CrossLegsModalPageModule {}
