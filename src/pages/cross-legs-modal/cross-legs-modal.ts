import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Store } from "@ngrx/store";
import { UserPreferenceDataState, UserPreferenceData } from '../../reducers/user-preferences/user-preferences.state';
import { getUserPreferenceDataStateContents } from '../../reducers';
import * as UserPreferenceAction from '../../reducers/user-preferences/user-preferences.actions'
import { ToastProvider } from '../../providers/toast/toast.provider';
/**
 * Generated class for the CrossLegsModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-cross-legs-modal',
  templateUrl: 'cross-legs-modal.html',
})
export class CrossLegsModalPage {
  // stores user-preference data to be used on the view
  userPreferenceData: UserPreferenceData;
  constructor(
  	public navCtrl: NavController,
  	public navParams: NavParams,
  	private view: ViewController,
    public userPreferenceStore: Store<UserPreferenceDataState>,
    public toast: ToastProvider) {
    // get user-preference data
    userPreferenceStore.select(getUserPreferenceDataStateContents).subscribe(
      (data: UserPreferenceData) => {
        this.userPreferenceData = data;
      },
      error => {
        this.toast.errorMessage('ERROR_CANT_GET_USER_PREFERENCE_DATA');
      }
    );
  }

  /**
   * @description get user preference data from store
   */
  ionViewDidLoad() {
    this.userPreferenceStore.dispatch({type:UserPreferenceAction.USER_PREFERENCE_GET_DATA, payload:{}});
  }

  /**
   * @description to store track posture user preference data into store
   */  
  storeTrackPosturePreference(trackPostureData)
  {
    this.userPreferenceStore.dispatch({type:UserPreferenceAction.USER_PREFERENCE_SET_DATA, payload:{trackPosture: trackPostureData.trackPosture, trackBadPosture: this.userPreferenceData.trackBadPosture, isTrackPromptFirstTime: false }});
    this.view.dismiss();
  }

}
