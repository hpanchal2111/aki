import { Store } from '@ngrx/store';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { SignupPage } from '../signup/signup';
import { AuthProvider } from '../../providers/auth/auth';
import { ToastProvider} from "../../providers/toast/toast.provider";
import { PairDevicePage } from '../pair-device/pair-device';
import * as UserActions from '../../reducers/user/user.actions';
import { SessionApiProvider } from "../../providers/api/session/session-api.provider";
/**
 * Generated class for the AuthenticationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-authentication',
  templateUrl: 'authentication.html',
})
export class AuthenticationPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public authProvider: AuthProvider,
    public toastProvider: ToastProvider,
    public store: Store<any>,
    public sessionApiProvider: SessionApiProvider
  )
    {
  }

  /**
   * @description Redirect to login page
   */
  redirectToLoginPage() {
    this.navCtrl.push(LoginPage);
  }
  /**
   * @description Redirect to signup page
   */
  redirectToSignupPage() {
    this.navCtrl.push(SignupPage);
  }

  /**
   * @description facebook login
   */
  fblogin(){
    this.authProvider.fblogin({'success': true}).subscribe(
      res => {
        // TODO replace the sotre dispatch with the proper user id
        this.store.dispatch({type: UserActions.USER_GET_USER, payload: 1})
        this.sessionApiProvider.getJWTToken();
        this.navCtrl.push(PairDevicePage);
        //this.navCtrl.push(TabsPage);
      },
      err => {
        this.toastProvider.errorMessage('ERROR_CANT_FBLOGIN');
      }
    );
  }

  /**
   * @description google plus login
   */
  gpluslogin(){
    this.authProvider.gpluslogin({'success': true}).subscribe(
      res => {
        // TODO replace the sotre dispatch with the proper user id
        this.store.dispatch({type: UserActions.USER_GET_USER, payload: 1})
        this.sessionApiProvider.getJWTToken();
        this.navCtrl.push(PairDevicePage);
        // this.navCtrl.push(TabsPage);
      },
      err => {
        this.toastProvider.errorMessage('ERROR_CANT_GPLUSLOGIN');
      }
    );
  }
}
