import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AuthProvider } from '../../providers/auth/auth';
import { ToastProvider} from "../../providers/toast/toast.provider";
import { PairDevicePage } from '../pair-device/pair-device';
import { Store } from '@ngrx/store';
import * as UserActions from '../../reducers/user/user.actions';
import { SessionApiProvider } from "../../providers/api/session/session-api.provider";
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  loginForm: FormGroup;

  constructor(
    public navCtrl: NavController,
    private formBuilder: FormBuilder,
    public authProvider: AuthProvider,
    public store: Store<any>,
    public toastProvider: ToastProvider,
    public sessionApiProvider: SessionApiProvider
    ) {
    this.loginForm = this.formBuilder.group({
      name: [''],
      password: ['']
    });
  }

  /**
   * @description Redirect to Back
   */
  goBack(){
    this.navCtrl.pop();
  }

  /**
   * @description call login using authprovider
   */
  login() {
    this.authProvider.login(this.loginForm.value).subscribe(
      res => {
        this.navCtrl.push(PairDevicePage);
        this.store.dispatch({type: UserActions.USER_GET_USER, payload: 1})
        this.sessionApiProvider.getJWTToken();
      },
      err => {
        this.toastProvider.errorMessage('ERROR_CANT_LOGIN');
      }
    );
  }

}
