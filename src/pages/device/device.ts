import { Component, NgZone, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/*import { BLE } from '@ionic-native/ble';
import { ToastProvider } from '../../providers/toast/toast.provider';*/
import {DeviceModel} from "../../models/device.model";

@Component({
  selector: 'page-device',
  templateUrl: 'device.html'
})
export class DevicePage implements OnInit {
  device: DeviceModel;
  statusMessage: string;
  testData: any;
  color = "greyColor";
  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    //private ble: BLE,
    //private toast: ToastProvider,
    private ngZone: NgZone,
  ) {
  }
  ngOnInit(){
    /*let device = this.navParams.get('device');
    this.ble.connect(device.id).subscribe(
      device => this.onConnected(device),
      device => this.toast.onDeviceDisconnected(device)
    );*/
  }

  onConnected(device) {
    this.ngZone.run(() => {
      this.device = device;
      // this.bleProvider.readDevice(peripheral);
    });
  }

}
