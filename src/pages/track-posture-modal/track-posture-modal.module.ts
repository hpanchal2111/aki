import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TrackPostureModalPage } from './track-posture-modal';

@NgModule({
  declarations: [
    TrackPostureModalPage,
  ],
  imports: [
    IonicPageModule.forChild(TrackPostureModalPage),
  ],
})
export class TrackPostureModalPageModule {}
