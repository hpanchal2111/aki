import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController } from 'ionic-angular';
import { Store } from "@ngrx/store";
import { UserPreferenceDataState, UserPreferenceData } from '../../reducers/user-preferences/user-preferences.state';
import { getUserPreferenceDataStateContents } from '../../reducers';
import * as UserPreferenceAction from '../../reducers/user-preferences/user-preferences.actions'
import { ToastProvider } from '../../providers/toast/toast.provider';
/**
 * Generated class for the TrackposturemodalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-track-posture-modal',
  templateUrl: 'track-posture-modal.html',
})
export class TrackPostureModalPage {
  // stores user-preference data to be used on the view
  userPreferenceData: UserPreferenceData;
  constructor(
  	public navCtrl: NavController,
  	private view: ViewController,
    public userPreferenceStore: Store<UserPreferenceDataState>,
    public toast: ToastProvider) {

    // get user-preference data
    userPreferenceStore.select(getUserPreferenceDataStateContents).subscribe(
      (data: UserPreferenceData) => {
        this.userPreferenceData = data;
      },
      error => {
        this.toast.errorMessage('ERROR_CANT_GET_USER_PREFERENCE_DATA');
      }
    );
  }

  /**
   * @description get user preference data from store
   */
  ionViewDidLoad() {
    this.userPreferenceStore.dispatch({type:UserPreferenceAction.USER_PREFERENCE_GET_DATA, payload:{}});
  }

  /**
   * @description to store track posture user preference data into store
   */  
  storeTrackPosturePreference(trackPostureData)
  {
    let data:UserPreferenceData = this.userPreferenceData;
    data = Object.assign(data, {trackPosture: trackPostureData.trackPosture, trackBadPosture: this.userPreferenceData.trackBadPosture, isTrackPromptFirstTime: false });
    this.userPreferenceStore.dispatch({type:UserPreferenceAction.USER_PREFERENCE_SET_DATA, payload:data});
    this.view.dismiss();
  }

}
