import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
/**
 * Generated class for the TroublepairdevicePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-trouble-pair-device',
  templateUrl: 'trouble-pair-device.html',
})
export class TroublePairDevicePage {

  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams) {
  }

}
