import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TroublePairDevicePage } from './trouble-pair-device';

@NgModule({
  declarations: [
    TroublePairDevicePage,
  ],
  imports: [
    IonicPageModule.forChild(TroublePairDevicePage),
  ],
})
export class TroublePairDevicePageModule {}
