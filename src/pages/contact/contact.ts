import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { PubNubProvider } from '../../providers/pub-nub/pub-nub';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  //pubnub: any;

  constructor(
    public navCtrl: NavController,
    public pub: PubNubProvider
  ) {

    // Uncomment to test pubnub integration
    // this.pubnub = pub.init();
    // this.pubnub.subscribe({ channels: ['myChannel'], triggerEvents: true, withPresence: true });

    // setInterval(() => {
    //   this.pubnub.publish(
    //     {
    //       channel: 'myChannel',
    //       message: Date.now()
    //     },
    //     (response) => {
    //       console.log('Response: ', response);
    //     }
    //   );

    //   this.pubnub.getMessage('myChannel', (msg) => {
    //     console.log('Get msg: ', msg);
    //   });

    // }, 5000);

  }

}
