import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { DevicePage } from '../device/device';
import { DeviceDiscoveredState } from "../../reducers/devices/discovered/device-discovered.state";
import { ToastProvider } from '../../providers/toast/toast.provider';
import { DevicePollingProvider } from "../../providers/device-polling/device-polling.provider";
import { Store } from '@ngrx/store';
import { UserPreferencesProvider } from "../../providers/resource/profile/user-preferences.provider";
import { UserPreferenceDataState, UserPreferenceData } from '../../reducers/user-preferences/user-preferences.state';
import { getUserPreferenceDataStateContents } from '../../reducers';
import * as UserPreferenceAction from '../../reducers/user-preferences/user-preferences.actions';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {
  devices: any;
  pubnub: any;
  // Totally fake
  user: Object;
  dataLoading = true;
  color = "greyColor";
  // stores user-preference data to be used on the view
  userPreferenceData: UserPreferenceData;

  constructor(
    public navCtrl: NavController,
    public toastProvider: ToastProvider,
    private devicePollingProvider:DevicePollingProvider,
    private storage: Storage,
    public userPreferencesProvider: UserPreferencesProvider,
    public userPreferenceStore: Store<UserPreferenceDataState>,
  ) {
    this.userPreferenceStore.dispatch({type:UserPreferenceAction.USER_PREFERENCE_GET_DATA, payload:{}});  
    // get user-preference data
    this.userPreferenceStore.select(getUserPreferenceDataStateContents).subscribe(
      (data: UserPreferenceData) => {
        this.userPreferenceData = data;
      },
      error => {
        this.toastProvider.errorMessage('ERROR_CANT_GET_USER_PREFERENCE_DATA');
      }
    );       
  }

  /**
   * @description on home page load open track posture modal for testing
   */  
  ionViewDidEnter() {
    //this.scan();
    // check if user preferences set for bad posture alert       
    /*if( this.userPreferenceData.isTrackPromptFirstTime == true )
    {
      this.userPreferencesProvider.trackPostureModal();
    } */
  }

  ngOnInit() {
    this.storage.get('deviceInfo').then((val) => {
  });
  }

  setCurrentUser(user: Object) {
    this.dataLoading = false;
    this.user = user;
  }

  error(error) {
    this.dataLoading = false;
    this.toastProvider.errorMessage(error);
  }

  setDevice(data:DeviceDiscoveredState) {
    /*let newArray: Array<DeviceModel>;
    for (const key in data.devices) {
      console.log(this.devices[key]);
      newArray.push(this.devices[key])
     }
    this.devices = newArray;*/
  }

  /**
   * scans for devices using the ble provider
   */
  scan() {
    this.devicePollingProvider.scan();
  }


  deviceSelected(device) {
    console.log(JSON.stringify(device) + ' selected');
    this.navCtrl.push(DevicePage, {
      device: device
    });
  }

}
