import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Platform } from 'ionic-angular';

/**
 * Generated class for the ShareReportModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-share-report-modal',
  templateUrl: 'share-report-modal.html',
})
export class ShareReportModalPage {

  today: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private view: ViewController,
    private platform: Platform
  ) {

    // close modal on press of back button
    this.platform.registerBackButtonAction(()=>{
      this.view.dismiss();
    });

    // get today's date
    this.today = new Date().toISOString().slice(0, 10);
  }

  /**
   * @description Close modal
   */
  close() {
    this.view.dismiss();
  }

}
