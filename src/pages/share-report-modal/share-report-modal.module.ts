import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShareReportModalPage } from './share-report-modal';

@NgModule({
  declarations: [
    ShareReportModalPage,
  ],
  imports: [
    IonicPageModule.forChild(ShareReportModalPage),
  ],
})
export class ShareReportModalPageModule {}
