import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { BarChartConstants, LineChartConstants, PieChartConstants, PolarChartConstants } from '../../constants/charts.constants';
// import { Store } from "@ngrx/store";
// import { DashboardDataState, DashboardData } from '../../reducers/dashboard/dashboard.state';
// import { getDashboardDataStateContents } from '../../reducers';
// import * as DashboardAction from '../../reducers/dashboard/dashboard.actions';
import { ToastProvider } from '../../providers/toast/toast.provider';
import {
  ChangeDetectorRef,
  ViewChild,
  trigger,
  transition,
  style,
  state,
  animate,
  keyframes
} from '@angular/core';
import { Slides } from 'ionic-angular';
import { UserPreferencesProvider } from '../../providers/resource/profile/user-preferences.provider';

/**
 * Generated class for the ViewReportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-report',
  templateUrl: 'view-report.html',
  animations: [
    trigger('bounce', [
      state('*', style({
        transform: 'translateX(0)'
      })),
      transition('* => rightSwipe', animate('700ms ease-out', keyframes([
        style({ transform: 'translateX(0)', offset: 0 }),
        style({ transform: 'translateX(-65px)', offset: .3 }),
        style({ transform: 'translateX(0)', offset: 1 })
      ]))),
      transition('* => leftSwipe', animate('700ms ease-out', keyframes([
        style({ transform: 'translateX(0)', offset: 0 }),
        style({ transform: 'translateX(65px)', offset: .3 }),
        style({ transform: 'translateX(0)', offset: 1 })
      ])))
    ])
  ]
})
export class ViewReportPage {

  @ViewChild(Slides) slides: Slides;
  state: string = 'x';
  //skipMessage: string = 'Skip';

  // initialize chart data using constants
  barChartData = BarChartConstants.DATA;
  lineChartData = LineChartConstants.DATA;
  pieChartData = PieChartConstants.DATA;
  polarChartData = PolarChartConstants.DATA;

  constructor(
    public navCtrl: NavController,
    // public dashboardStore: Store<DashboardDataState>,
    public toast: ToastProvider,
    private cdr: ChangeDetectorRef,
    public userPreferencesProvider: UserPreferencesProvider,
  ) {

    // commented code as per jefren suggestion
    // get dashboard data 
    /*dashboardStore.select(getDashboardDataStateContents).subscribe(
      (dataDash: DashboardData) => {

        // set bar chart data from store
        this.barChartData.labels = dataDash.barChartData.labels;
        this.barChartData.datasets[0].data = dataDash.barChartData.data;

        // set line chart data from store
        this.lineChartData.labels = dataDash.lineChartData.labels;
        this.lineChartData.datasets[0].data = dataDash.lineChartData.data;

        // set pie chart data from store
        this.pieChartData.labels = dataDash.pieChartData.labels;
        this.pieChartData.datasets[0].data = dataDash.pieChartData.data;

        // set polar chart data from store
        this.polarChartData.labels = dataDash.polarChartData.labels;
        this.polarChartData.datasets[0].data = dataDash.polarChartData.data;
      },
      error => {
        this.toast.errorMessage('ERROR_CANT_GET_DASHBOARD_DATA');
      }
    );*/
  }

  ionViewDidEnter() {
    // this.dashboardStore.dispatch({ type: DashboardAction.DASHBOARD_GET_DATA, payload: {} })
  }

  /**
 * @description check the direction user is swiping in to attach the state to the animation module
 */
  slideMoved() {
    if (this.slides.getActiveIndex() >= this.slides.getPreviousIndex()) {
      this.state = 'rightSwipe';
    }
    else {
      this.state = 'leftSwipe';
    }
    // Check for changes to the dom on swipes, otherwise will throw an error when switching slides and stuff
    this.cdr.detectChanges();
  }

  animationDone() {
    this.state = 'x';
  }

  /** @description Show share report modal */
  showShareReportModal() {
    this.userPreferencesProvider.shareReportModal();
  }

}
