import {
  Component,
  ChangeDetectorRef,
  ViewChild,
  trigger,
  transition,
  style,
  state,
  animate,
  keyframes
} from '@angular/core';
import { IonicPage, NavController, Slides } from 'ionic-angular';
import { TestWalkPage } from '../test-walk/test-walk';

/**
 * Generated class for the OnboardingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-onboarding',
  templateUrl: 'onboarding.html',
  animations: [
    trigger('bounce', [
      state('*', style({
        transform: 'translateX(0)'
      })),
      transition('* => rightSwipe', animate('700ms ease-out', keyframes([
        style({transform: 'translateX(0)', offset: 0}),
        style({transform: 'translateX(-65px)', offset: .3}),
        style({transform: 'translateX(0)', offset: 1})
      ]))),
      transition('* => leftSwipe', animate('700ms ease-out', keyframes([
        style({transform: 'translateX(0)', offset: 0}),
        style({transform: 'translateX(65px)', offset: .3}),
        style({transform: 'translateX(0)', offset: 1})
      ])))
    ])
  ]
})
export class OnboardingPage {
  @ViewChild(Slides) slides: Slides;
  state: string = 'x';
  skipMessage: string = 'Skip';
  
  constructor(
    public navCtrl: NavController,
    private cdr: ChangeDetectorRef
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad OnboardingPage');
  }

  /**
   * @description check the direction user is swiping in to attach the state to the animation module
   */
  slideMoved() {
    if (this.slides.getActiveIndex() >= this.slides.getPreviousIndex()) {
      this.state = 'rightSwipe';
    }
    else {
      this.state = 'leftSwipe';
    }
    // Check for changes to the dom on swipes, otherwise will throw an error when switching slides and stuff
    this.cdr.detectChanges();
  }

  /**
   * @description Check if the user reached the end of the onboarding to switch the skip message to done message
   */
  slideChanged() {
    if (this.slides.isEnd())
      this.skipMessage = 'Sweet! Let\'s get started!';
  }

  animationDone() {
    this.state = 'x';
  }

  skip() {
    this.navCtrl.push(TestWalkPage);
  }
}
