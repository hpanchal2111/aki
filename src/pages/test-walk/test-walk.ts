import { Component } from '@angular/core';
import 'rxjs/add/operator/filter';
import { IonicPage, NavController } from 'ionic-angular';
import { Store } from '@ngrx/store';
import { getUserPreferenceDataStateContents} from '../../reducers';
import { ToastProvider } from '../../providers/toast/toast.provider';
import { AlertEffects } from "../../effects/alert.effects";
import * as UserPreferenceActions from '../../reducers/user-preferences/user-preferences.actions';
import * as AlertActions from '../../reducers/alert/alert.actions';
import { AlertConstants } from "../../constants/alert.constants";
import { Subscription } from "rxjs/Subscription";
import { Observable } from 'rxjs/Rx';
import { DashboardConstants } from "../../constants/dashboard.constants";
import { TabsPage } from "../tabs/tabs";
import { UserPreferenceData } from "../../reducers/user-preferences/user-preferences.state";
/**
 * Generated class for the TestWalkPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-test-walk',
  templateUrl: 'test-walk.html',
})
export class TestWalkPage {
  // stores user data to be used on the view
  userPreferenceData: UserPreferenceData;
  private subscription: Subscription;
  private stepsTaken:number = 0;

  constructor(
  	public navCtrl: NavController,
  	public store: Store<any>,
  	public toast: ToastProvider,
    public alertEffects : AlertEffects
  ) {
    /**
    * @description get user store data
    */
    store.select(getUserPreferenceDataStateContents).subscribe(
      (data: UserPreferenceData) => {
        this.userPreferenceData = data;
      },
      error => {
        this.toast.errorMessage('USER_GET_USER_ERROR');
      }
    );  
    /**
    * @description update test walk steps for test walk page
    */
    this.subscription = alertEffects.alert
      .filter(action => action.type === AlertActions.ADD_ALERT_HANDLED)
      .subscribe(action => {if(action.payload[0].message ===  'WALKING_STEP_TAKEN') {
        this.testWalkCountStep(action);
      }});    
  }
          
  /**
   * @description update test walk steps for test walk page
   */
  testWalkCountStep(action):void
  {
    // check if test walk started
    //if(this.userData.startTestWalk)
    //{
      // this.userData.testWalkSteps += 1;
      this.stepsTaken += 1;
      //this.userStore.dispatch({type:UserActions.USER_SET_DATA, payload: this.userData});
      if(this.stepsTaken == AlertConstants.TEST_WALK_STEP)
      {
        const message:string = action.payload[0].message;
        const toast:boolean = action.payload.hasOwnProperty('toast')?action.payload[0].toast:true;
        const localNotification:boolean = false;
        const interpolateParams:Object = action.payload.hasOwnProperty('interpolateParams')?action.payload[0].interpolateParams:{};        
        this.testWalkSuccess(message, interpolateParams, toast, localNotification);
      }      
    //}
  }

  /**
   * @description test walk successfully completed
   */
  testWalkSuccess(message:string, interpolateParams:Object = {}, toast:boolean, localNotification:boolean):void
  {
    //if(!this.userData.testWalkSuccess){
      //this.userData.startTestWalk = false;
      this.userPreferenceData.testWalkSuccess = true;
      this.store.dispatch({type:UserPreferenceActions.USER_PREFERENCE_SET_DATA, payload: this.userPreferenceData});
      this.alertEffects.displayMessage(message, interpolateParams, toast, localNotification);
      // go to dashboard page after 5 seconds
      let redirect_to_dashboard_timeout = Observable.timer(DashboardConstants.DASHBOARD_REDIRECT_TIMEOUT);
      redirect_to_dashboard_timeout.subscribe(x =>{
        // go to Dashboard tab with tabs
        this.navCtrl.push(TabsPage);
        //this.navCtrl.parent.select(DashboardConstants.DASHBOARD_INDEX);
      });
    //}
  }

  /**
   * @description unsubscribe alertEffects alert
   */
  ionViewDidLeave()
  {
    this.subscription.unsubscribe();
  }
}
