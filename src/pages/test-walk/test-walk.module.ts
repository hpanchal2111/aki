import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TestWalkPage } from './test-walk';

@NgModule({
  declarations: [
    TestWalkPage,
  ],
  imports: [
    IonicPageModule.forChild(TestWalkPage),
  ],
})
export class TestWalkPageModule {}
