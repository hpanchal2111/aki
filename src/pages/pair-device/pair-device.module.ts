import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PairDevicePage } from './pair-device';

@NgModule({
  declarations: [
    PairDevicePage,
  ],
  imports: [
    IonicPageModule.forChild(PairDevicePage),
  ],
})
export class PairdevicePageModule {}
