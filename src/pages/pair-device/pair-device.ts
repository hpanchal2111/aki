import { Component, Input } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { Store } from "@ngrx/store";
import { DeviceInfoState } from '../../reducers/devices/info/device-info.state';
import { DeviceDiscoveredState } from "../../reducers/devices/discovered/device-discovered.state";
import { getDevicesToPair } from '../../reducers';
import { getDevicesSignalToPairUpdate } from '../../reducers';
import { getDeviceDiscoveredStateContents } from '../../reducers';
import { ToastProvider } from '../../providers/toast/toast.provider';
import { TroublePairDevicePage } from '../trouble-pair-device/trouble-pair-device';
import { PairDeviceSuccessPage } from '../pair-device-success/pair-device-success';
import { DeviceInterface } from "../../interfaces/models/device.interface";
import 'rxjs/add/operator/first';
/**
 * Generated class for the PairdevicePage page.
 *
 * See https://ionicframework.com/docs/components/#navigatiDeviceInfoStateon for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pair-device',
  templateUrl: 'pair-device.html',
})
export class PairDevicePage {

  /**
   * @description stores scanned devices data
   */
  @Input() devices: DeviceDiscoveredState;

  protected movedOn:boolean = false;
  protected isPairing:boolean = false;

  constructor(
    public navCtrl: NavController,
    public deviceInfoStore: Store<DeviceInfoState>,
    public deviceDiscoveredStore: Store<DeviceDiscoveredState>,
    public toastProvider: ToastProvider
  ) {
    /**
       * @description get scanned devices data
       */
    this.deviceDiscoveredStore.select(getDeviceDiscoveredStateContents)
      .subscribe(
        (data: DeviceDiscoveredState) => {
          this.devices = data;
        },
        error => {
          this.toastProvider.errorMessage('ERROR_DEVICE_NOT_FOUND')
        }
      );

  }

  ionViewDidEnter() {
    this.movedOn = false;
    this.isPairing = false;
    /**
    * @description fetch single device data from multiple device list
    */
    this.deviceInfoStore.select(getDevicesToPair)
      .subscribe(
        (data: Array<string>) => {
          // check if we have fethced a single device and have stored scanned devices
          if (this.movedOn === false && data.length > 0 && this.devices.hasOwnProperty(data[0])) {
            let device: DeviceInterface = this.devices[data[0]];
            if (device) {
              device.isPaired = true;
              this.movedOn = true;
              this.navCtrl.push(PairDeviceSuccessPage);
            }
            else {
              this.movedOn = true;
              this.navCtrl.push(TroublePairDevicePage);
            }
          }
        },
        error => {
          this.toastProvider.errorMessage('ERROR_DEVICE_NOT_FOUND')
        }
      );

    this.deviceInfoStore.select(getDevicesSignalToPairUpdate)
      .subscribe(
        (data: boolean) => {  
          this.isPairing = data;
        }
      );    
  }

}
