import { Component } from '@angular/core';

import { HomePage } from '../home/home';
import { DashboardPage } from '../dashboard/dashboard';
import { SettingsPage } from '../settings/settings';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  homeTab = HomePage;
  settingsTab = SettingsPage;
  dashboardTab = DashboardPage;

  constructor() {

  }
}
