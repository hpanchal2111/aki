import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { Store } from "@ngrx/store";
import { DashboardDataState, DashboardData } from '../../reducers/dashboard/dashboard.state';
import {getDashboardDataStateContents, getUserPreferenceDataStateContents} from '../../reducers';
import { ToastProvider } from '../../providers/toast/toast.provider';
import * as DashboardAction from '../../reducers/dashboard/dashboard.actions'
import introJs from 'intro.js/intro.js';
import { TranslateService } from '@ngx-translate/core';
import { ViewReportPage } from '../view-report/view-report';
import * as UserPreferenceAction from "../../reducers/user-preferences/user-preferences.actions";
import {UserPreferenceData, UserPreferenceDataState} from "../../reducers/user-preferences/user-preferences.state";
import {UserPreferencesProvider} from "../../providers/resource/profile/user-preferences.provider";

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {

  // stores dashboard data to be used on the view
  dashboardData: DashboardData;

  // stores user-preference data to be used on the view
  userPreferenceData: UserPreferenceData;

  constructor(
    public navCtrl: NavController,
    public dashboardStore: Store<DashboardDataState>,
    public toast: ToastProvider,
    public translate: TranslateService,
    public toastProvider: ToastProvider,
    public userPreferencesProvider: UserPreferencesProvider,
    public userPreferenceStore: Store<UserPreferenceDataState>,
  ) {

    // get dashboard data
    dashboardStore.select(getDashboardDataStateContents).subscribe(
      (data: DashboardData) => {
        this.dashboardData = data;
      },
      error => {
        this.toast.errorMessage('ERROR_CANT_GET_DASHBOARD_DATA');
      }
    );

    this.userPreferenceStore.dispatch({type:UserPreferenceAction.USER_PREFERENCE_GET_DATA, payload:{}});
    // get user-preference data
    this.userPreferenceStore.select(getUserPreferenceDataStateContents).subscribe(
      (data: UserPreferenceData) => {
        this.userPreferenceData = data;
      },
      error => {
        this.toastProvider.errorMessage('ERROR_CANT_GET_USER_PREFERENCE_DATA');
      }
    );
  }

  ionViewDidEnter() {
    this.dashboardStore.dispatch({ type: DashboardAction.DASHBOARD_GET_DATA, payload: {} })
    if( this.userPreferenceData.isTrackPromptFirstTime == true )
    {
      //this.userPreferencesProvider.trackPostureModal();
    }
  }

  ngAfterViewInit() {
    if(this.userPreferenceData.isTrackPromptFirstTime)
      this.onboarding(); 
  }

  /**
   * @description Initialize dashboard onboarding
   */
  onboarding() {
    let intro = introJs.introJs();
    let obj = this;
    intro.setOptions({
      steps: [
        {
          element: '#step1',
          intro: this.translate.instant('DASHBOARD_STEP_ONE_ONBOARDING')
        },
        {
          element: '#step2',
          intro: this.translate.instant('DASHBOARD_STEP_TWO_ONBOARDING')
        },
        {
          element: '#step3',
          intro: this.translate.instant('DASHBOARD_STEP_THREE_ONBOARDING')
        }
      ],
      tooltipClass: 'custom-tooltipClass',
      highlightClass: 'custom-highlightClass'
    });
    intro.start();
    /**
     * @description Open bad posture prompt after intro complete or onexit event
    */      
    intro.oncomplete(function () {
      obj.userPreferencesProvider.trackPostureModal();
    });

    intro.onexit(function () {
      obj.userPreferencesProvider.trackPostureModal();
    });    
  }
  
  /**
   * @description Redirects page to View Report Page
   */
  viewReport() {
    this.navCtrl.push(ViewReportPage);
  }

  /**@description Share result */
  shareResult() {
    //TODO share result pop up
  }
}
