import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PairDeviceSuccessPage } from './pair-device-success';

@NgModule({
  declarations: [
    PairDeviceSuccessPage,
  ],
  imports: [
    IonicPageModule.forChild(PairDeviceSuccessPage),
  ],
})
export class PairDeviceSuccessPageModule {}
