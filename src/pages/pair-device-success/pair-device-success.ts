import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { OnboardingPage } from '../onboarding/onboarding';
/**
 * Generated class for the PairdevicesuccessPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-pair-device-success',
  templateUrl: 'pair-device-success.html',
})
export class PairDeviceSuccessPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams) {

    // go to onboarding page after 5 seconds
    setTimeout(() => {
      this.navCtrl.push(OnboardingPage);
    }, 5000);

  }
}
