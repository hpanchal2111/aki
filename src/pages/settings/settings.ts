import { OnboardingPage } from '../onboarding/onboarding';
import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { TestWalkPage } from '../test-walk/test-walk';
import { Store } from "@ngrx/store";
import { UserDataState, UserData } from '../../reducers/user/user.state';
import { getUserDataStateContents } from '../../reducers';
import { ToastProvider } from '../../providers/toast/toast.provider';
/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {
  // stores user data to be used on the view
  userData: UserData;
  color = "greyColor";
  constructor(
    public navCtrl: NavController,
    public userStore: Store<UserDataState>,
    public toast: ToastProvider
  ) {
    // get user data
    userStore.select(getUserDataStateContents).subscribe(
      (data: UserData) => {
        this.userData = data;
      },
      error => {
        this.toast.errorMessage('USER_GET_USER_ERROR');
      }
    );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }

  startOnboarding() {
    this.navCtrl.push(OnboardingPage);
  }
  /**
   * @description Redirect to Test Walk page
   */
  redirectToTestWalkPage() {
    //this.userData.startTestWalk = true;
    //this.userStore.dispatch({type:UserActions.USER_SET_DATA, payload: this.userData});
    this.navCtrl.push(TestWalkPage);
  }
}
