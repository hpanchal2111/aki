import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AuthProvider } from '../../providers/auth/auth';
import { ToastProvider } from "../../providers/toast/toast.provider";
import { PairDevicePage } from '../pair-device/pair-device';
import { SessionApiProvider } from "../../providers/api/session/session-api.provider";
/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  signupForm: FormGroup;

  constructor(
    public navCtrl: NavController,
    private formBuilder: FormBuilder,
    public authProvider: AuthProvider,
    public toastProvider: ToastProvider,
    public sessionApiProvider: SessionApiProvider
    ) {
    this.signupForm = this.formBuilder.group({
      name: [''],
      password: ['']
    });
  }

  /**
   * @description Redirect to Back
   */
  goBack(){
    this.navCtrl.pop();
  }

  /**
   * @description call sigup using authprovider
   */
  signup()
  {
    this.authProvider.signup(this.signupForm.value).subscribe(
      res => {
        this.navCtrl.push(PairDevicePage);
        this.sessionApiProvider.getJWTToken();
      },
      err => {
        this.toastProvider.errorMessage('ERROR_CANT_SIGNUP');
      }
    );
  }
}
