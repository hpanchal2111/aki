import { Observable } from 'rxjs/Observable';
import { UserProvider } from '../providers/resource/profile/user.provider';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map, catchError, switchMap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { Injectable } from '@angular/core';
import * as UserActions from '../reducers/user/user.actions'
import { ToastProvider } from '../providers/toast/toast.provider';

//import {ToastProvider} from '../../providers/toast/toast.provider'
@Injectable()
export class UserEffects {
  constructor(
    private actions: Actions,
    private toast: ToastProvider,
    private userProvider:UserProvider
  ) { }

  /**
   * Set the user data to the server
   * @type {Observable<any>}
   */
  @Effect() userDataSet: Observable<any> = this.actions.pipe(
    ofType(UserActions.USER_SET_DATA),
    switchMap((action: any) => this.userProvider.update('', action.payload)
      .map(
        res => ({type: UserActions.USER_SET_DATA_HANDLED, payload: action.payload})
      )
    ),
    catchError( error => of({type: UserActions.USER_SET_DATA_ERROR, payload: error}))
  );

  /**
   * @description
   */
  @Effect() user: Observable<any> = this.actions.pipe(
    ofType(UserActions.USER_GET_USER),
    switchMap(
      (action: any) => this.getUser(action)
    )
  );

  @Effect() userError: Observable<any> = this.actions.pipe(
    ofType(UserActions.USER_GET_USER_ERROR),
    map((action: any) => this.error(action, UserActions.USER_GET_USER_ERROR_HANDLED))
  );

  protected getUser(action) {
    return this.userProvider.get(action.payload).pipe(
      map((data: any) => {
        return ({type: UserActions.USER_GET_DATA_HANDLED, payload: data, loaded: true})
      }),
      catchError( error => of({type: UserActions.USER_GET_USER_ERROR, payload: error}))
    )
  }

  protected error(error, type) {
    this.toast.errorMessage(error);
    return ({type: type});
  }
}
