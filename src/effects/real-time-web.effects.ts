import { Observable } from 'rxjs/Observable';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map, switchMap, catchError } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { Injectable } from '@angular/core';
import * as RealTimeWebActions from '../reducers/real-time-web/real-time-web.actions';
import { PubNubProvider, PubNubSubscribeArgs } from "../providers/pub-nub/pub-nub";
import { ActionWithPayload } from "../interfaces/actions/action-with-payload.interface";
import { RealTimeWebData } from "../reducers/real-time-web/real-time-web.state";
import { PubNubConstants } from "../constants/pubnub.constants";
import { ToastProvider } from "../providers/toast/toast.provider";
import { Store } from '@ngrx/store';
import { Subscription } from "rxjs/Subscription";
import { getRealTimeWebContents } from "../reducers";
import { MessageInterface } from "../interfaces/message/message.interface";
import "rxjs/add/observable/of";

//import {ToastProvider} from '../../providers/toast/toast.provider'
@Injectable()
export class RealTimeWebEffects {
  constructor(
    private actions: Actions,
    private pubNub: PubNubProvider,
    private toast:ToastProvider,
    private store:Store<any>
  ) { }

  /**
   * Set the preference data to the server
   * @type {Observable<any>}
   */
  @Effect() rtwSubscribe: Observable<any> = this.actions.pipe(
    ofType(RealTimeWebActions.RTW_SUBSCRIBE),
    switchMap((action: ActionWithPayload<RealTimeWebData>) =>this.subscribe(action)
      /*.map(
        res => ({type: RealTimeWebActions.RTW_SUBSCRIBE_HANDLED, payload: res.payload})
      )*/
    ),
    catchError( error => of({type: RealTimeWebActions.RTW_SUBSCRIBE_ERROR, payload: error}))
  );

  /**
   * Handle error for subscribe
   * @type {Observable<{type: *}>}
   */
  @Effect() rtwSubscribeError: Observable<any> = this.actions.pipe(
    ofType(RealTimeWebActions.RTW_SUBSCRIBE_ERROR),
    map((action: any) => this.error(action, RealTimeWebActions.RTW_SUBSCRIBE_HANDLED))
  );

  /**
   * Set the preference data to the server
   * @type {Observable<any>}
   */
  @Effect() rtwUnsubscribe: Observable<any> = this.actions.pipe(
    ofType(RealTimeWebActions.RTW_UNSUBSCRIBE),
    switchMap((action: ActionWithPayload<RealTimeWebData>) =>this.unsubscribe(action)
      /*.map(
        res => ({type: RealTimeWebActions.RTW_UNSUBSCRIBE_HANDLED, payload: res.payload})
      )*/
    ),
    catchError( error => of({type: RealTimeWebActions.RTW_UNSUBSCRIBE_ERROR, payload: error}))
  );

  /**
   * Handle error for subscribe
   * @type {Observable<{type: *}>}
   */
  @Effect() rtwUnsubscribeError: Observable<any> = this.actions.pipe(
    ofType(RealTimeWebActions.RTW_UNSUBSCRIBE_ERROR),
    map((action: any) => this.error(action, RealTimeWebActions.RTW_UNSUBSCRIBE_HANDLED))
  );


  /**
   * Set the preference data to the server
   * @type {Observable<any>}
   */
  @Effect() rtwInit: Observable<any> = this.actions.pipe(
    ofType(RealTimeWebActions.RTW_INIT),
    switchMap((action: ActionWithPayload<RealTimeWebData>) =>this.init(action)
      /*.map(
        res => ({type: RealTimeWebActions.RTW_INIT_HANDLED, payload: res.payload})
      )*/
    ),
    catchError( error => of({type: RealTimeWebActions.RTW_INIT_ERROR, payload: error}))
  );

  /**
   * Handle error for subscribe
   * @type {Observable<{type: *}>}
   */
  @Effect() rtwInitError: Observable<any> = this.actions.pipe(
    ofType(RealTimeWebActions.RTW_INIT_ERROR),
    map((action: any) => this.error(action, RealTimeWebActions.RTW_INIT_ERROR_HANDLED))
  );

  /**
   * subscribes to a PubNub channels
   * @param action
   * @returns {Observable<any>}
   */
  protected subscribe (action: ActionWithPayload<RealTimeWebData>):Observable<any> {
    // Get current state
    let realTimeWebData:RealTimeWebData = this.getState();

    // Go through requested channels in payload
    for (const key in action.payload.channels) {
      // I need this ridiculous if statement to appease the linter
      if (action.payload.channels.hasOwnProperty(key) === true) {
        // Assign defaults
        action.payload.channels[key] = Object.assign(PubNubConstants.CHANNEL_DEFAULTS, action.payload.channels[key]);
        // If it's already there, and the settings match, remove it from the payload
        if (
          realTimeWebData.channels.hasOwnProperty(key) === true
          &&  JSON.stringify(realTimeWebData.channels[key]) === JSON.stringify(action.payload.channels[key])
        ) {
          delete action.payload.channels[key];
        }
      }
    }

    for (const key in action.payload.channels) {
      // I need this ridiculous if statement to appease the linter
      if (action.payload.channels.hasOwnProperty(key) === true) {
        // Make the args and pass them through PubNub subscribe

        let args:PubNubSubscribeArgs = action.payload.channels[key];
        args.channels = [key];

        console.log('PubNub subscribe', JSON.stringify(args));
        this.pubNub.subscribe(args);
        // Get messages on the channel
        this.pubNub.getMessage(key, (msg:MessageInterface) => {
          if (PubNubConstants.TYPE_ENDS_WITH_REGEX.test(msg.message.type)) {
            this.store.dispatch(msg.message)
          }
        });
      }
    }
    // This is the only way I can figure out to return data from here that seems like it would work.
    //return this.actions.map(res => ({type: RealTimeWebActions.RTW_SUBSCRIBE_HANDLED, payload: action.payload}));
    return Observable.of({type: RealTimeWebActions.RTW_SUBSCRIBE_HANDLED, payload: action.payload});
  }

  /**
   * unsubscribe to a PubNub channels
   * @param action
   * @returns {Observable<any>}
   */
  protected unsubscribe (action: ActionWithPayload<RealTimeWebData>):Observable<any> {
    // Get current state
    let realTimeWebData:RealTimeWebData = this.getState();

    // Go through requested channels in payload
    for (const key in action.payload.channels) {
      // I need this ridiculous if statement to appease the linter
      if (action.payload.channels.hasOwnProperty(key) === true) {
        if (
          realTimeWebData.channels.hasOwnProperty(key) === false
        ) {
          delete action.payload.channels[key];
        }
      }
    }

    for (const key in action.payload.channels) {
      // I need this ridiculous if statement to appease the linter
      if (action.payload.channels.hasOwnProperty(key) === true) {
        this.pubNub.unsubscribe(key);
      }
    }
    // This is the only way I can figure out to return data from here that seems like it would work.
    //return this.actions.map(res => ({type: RealTimeWebActions.RTW_UNSUBSCRIBE_HANDLED, payload: action.payload}));
    return Observable.of({type: RealTimeWebActions.RTW_UNSUBSCRIBE_HANDLED, payload: action.payload});
  }

  /**
   * unsubscribe to a PubNub channels
   * @param action
   * @returns {Observable<any>}
   */
  protected init (action: ActionWithPayload<RealTimeWebData>):Observable<any> {
    // Get current state
    let realTimeWebData:RealTimeWebData = this.getState();

    if (realTimeWebData.uuid !== action.payload.uuid) {
      console.log('PubNub init', JSON.stringify(action.payload.uuid));
      this.pubNub.init(action.payload.uuid);
    }
    // This is the only way I can figure out to return data from here that seems like it would work.
    //return this.actions.map(res => ({type: RealTimeWebActions.RTW_INIT_HANDLED, payload: action.payload}));
    return Observable.of({type: RealTimeWebActions.RTW_INIT_HANDLED, payload: action.payload});
  }


  /**
   * Get's the state from the store
   * @returns {RealTimeWebData}
   */
  protected getState():RealTimeWebData {
    let realTimeWebData:RealTimeWebData;
    let subscription:Subscription = this.store.select(getRealTimeWebContents).subscribe(s => realTimeWebData = s);
    subscription.unsubscribe();
    return realTimeWebData;
  }

  error(error, type) {
    this.toast.errorMessage(error.payload.message, false);
    return ({ type: type });
  }
}
