import { Observable } from 'rxjs/Observable';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map, mergeMap, switchMap, catchError } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { Injectable } from '@angular/core';
import * as UserPreferencesActions from '../reducers/user-preferences/user-preferences.actions';
import { ToastProvider } from '../providers/toast/toast.provider';
import { UserPreferencesProvider } from "../providers/resource/profile/user-preferences.provider";

//import {ToastProvider} from '../../providers/toast/toast.provider'
@Injectable()
export class UserPreferencesEffects {
  constructor(
    private actions: Actions,
    private toast: ToastProvider,
    private userPreferencesProvider: UserPreferencesProvider
  ) { }

  /**
   * Set the preference data to the server
   * @type {Observable<any>}
   */
  @Effect() userPreferenceSet: Observable<any> = this.actions.pipe(
    ofType(UserPreferencesActions.USER_PREFERENCE_SET_DATA),
    switchMap((action: any) => this.userPreferencesProvider.update('', action.payload)
      .map(
        res => ({type: UserPreferencesActions.USER_PREFERENCE_SET_DATA_HANDLED, payload: action.payload})
      )
    ),
    catchError( error => of({type: UserPreferencesActions.USER_PREFERENCE_SET_DATA_ERROR, payload: error}))
  );

  /**
   * @description
   */
  @Effect() userPreference: Observable<any> = this.actions.pipe(
    ofType(UserPreferencesActions.USER_PREFERENCE_GET_DATA),
    mergeMap(
      (action: any) => this.getUserPreference(action)
    )
  );

  @Effect() userPreferenceError: Observable<any> = this.actions.pipe(
    ofType(UserPreferencesActions.USER_PREFERENCE_GET_ERROR),
    map((action: any) => this.error(action, UserPreferencesActions.USER_PREFERENCE_GET_ERROR_HANDLED))
  );

  @Effect() userPreferenceErrorSet: Observable<any> = this.actions.pipe(
    ofType(UserPreferencesActions.USER_PREFERENCE_SET_DATA_ERROR),
    map((action: any) => this.error(action, UserPreferencesActions.USER_PREFERENCE_SET_DATA_ERROR_HANDLED))
  );

  protected getUserPreference(action) {
    return this.userPreferencesProvider.list(action.payload).pipe(
      map((data: any) => {
        return ({type: UserPreferencesActions.USER_PREFERENCE_GET_DATA_HANDLED, payload: data, loaded: true})
      }),
      catchError(error => of({ type: UserPreferencesActions.USER_PREFERENCE_GET_ERROR, payload: error }))
    )
  }

  error(error, type) {
    this.toast.errorMessage(error.payload.message, false);
    return ({ type: type });
  }
}
