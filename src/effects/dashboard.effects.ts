import { Effect, Actions } from "@ngrx/effects";
import { Injectable } from "@angular/core";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import * as DashboardAction from '../reducers/dashboard/dashboard.actions'
import { DashboardProvider } from "../providers/resource/machine-learning/dashboard.provider";
//import {ToastProvider} from "../../providers/toast/toast.provider";

@Injectable()
export class DashboardEffects {

  constructor(
    private actions: Actions,
    private dashboardProvider:DashboardProvider
  ) { }


  // TODO: Get error handling working
  @Effect() getDashboard = this.actions
  // Listen for the 'DASHBOARD_GET_DATA' action
    .ofType(DashboardAction.DASHBOARD_GET_DATA)
    // Map the payload into JSON to use as the request body
    .map(action => {})
    .switchMap(payload => this.dashboardProvider.list()
      // If successful, dispatch success action with result
        .map(res => ({ type: DashboardAction.DASHBOARD_SET_DATA, payload: res }))
        // If request fails, dispatch failed action
      //.catch(() => Observable.of({ type: 'LOGIN_FAILED' }))
  );

}
