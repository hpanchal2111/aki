import { Observable } from 'rxjs/Observable';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import * as AlertActions from '../reducers/alert/alert.actions'
import { ToastProvider } from '../providers/toast/toast.provider';
import { Store } from "@ngrx/store";
import { UserPreferencesProvider } from "../providers/resource/profile/user-preferences.provider";
import { UserPreferenceDataState, UserPreferenceData } from '../reducers/user-preferences/user-preferences.state';
import { UserDataState, UserData } from '../reducers/user/user.state';
import { getUserPreferenceDataStateContents } from '../reducers';
import { getUserDataStateContents } from '../reducers';
import { ActionWithPayload } from "../interfaces/actions/action-with-payload.interface";
import { AlertData } from "../reducers/alert/alert.state";
import { LocalNotificationProvider } from "../providers/local-notification/local-notification.provider";
import { BackgroundMode } from "@ionic-native/background-mode";
import { LocalNotificationConstants } from "../constants/local-notification.constants";
import 'rxjs/add/operator/share';
import { AlertConstants } from "../constants/alert.constants";

@Injectable()
export class AlertEffects {
  // stores user-preference data to be used on the view
  userPreferenceData: UserPreferenceData;
  // stores user data to be used on the view
  userData: UserData;

  constructor(
    private actions: Actions,
    private toast: ToastProvider,
    public userPreferencesProvider: UserPreferencesProvider,
    public userPreferenceStore: Store<UserPreferenceDataState>,
    public userStore: Store<UserDataState>,
    private localNotification:LocalNotificationProvider,
    private backgroundMode: BackgroundMode
  ) {
    // get user-preference data
    this.userPreferenceStore.select(getUserPreferenceDataStateContents).subscribe(
      (data: UserPreferenceData) => {
        this.userPreferenceData = data;
      },
      error => {
        this.toast.errorMessage('ERROR_CANT_GET_USER_PREFERENCE_DATA');
      }
    );
    // get user data
    userStore.select(getUserDataStateContents).subscribe(
      (data: UserData) => {
        this.userData = data;
      },
      error => {
        this.toast.errorMessage('USER_GET_USER_ERROR');
      }
    );  
  }

  /**
   * @description display a message when an ADD_ALERT_RTW is called
   */
  @Effect() alert: Observable<any> = this.actions.pipe(
    ofType(AlertActions.ADD_ALERT_RTW),
    map(
      (action: ActionWithPayload<AlertData>) => {
        const message:string = action.payload.message;
        const defaultToast:boolean = AlertConstants.ALERT_MESSAGE_NO_TOAST.indexOf(message) == -1;
        const defaultLocal:boolean = AlertConstants.ALERT_MESSAGE_NO_LOCAL.indexOf(message) == -1;
        const toast:boolean = action.payload.hasOwnProperty('toast')?action.payload.toast:defaultToast;
        const localNotification:boolean = action.payload.hasOwnProperty('localNotification')?action.payload.localNotification:defaultLocal;
        const interpolateParams:Object = action.payload.hasOwnProperty('interpolateParams')?action.payload.interpolateParams:{};
        // display track posture modal
        if(AlertConstants.TRACK_POSTURE_PROMPT.indexOf(message) !== -1)
        {
          this.displayTrackPostureModal(message, interpolateParams, toast, localNotification);
        } else if(toast === true || localNotification === true) {
          this.displayMessage(message, interpolateParams, toast, localNotification);
        }
        return {type: AlertActions.ADD_ALERT_HANDLED, payload:[action.payload]}
      }
    )
  ).share();

  /**
   * @description displays the message
   * @param {string} message
   * @param interpolateParams
   * @param toast
   * @param localNotification
   */
  displayMessage(message:string, interpolateParams:Object = {}, toast:boolean, localNotification:boolean):void {
    if (toast === true) {
      this.toast.successMessage(message, true, interpolateParams);
    }

    const isActive:boolean = this.backgroundMode.isActive();
    if (
        localNotification === true
        && (
            LocalNotificationConstants.SEND_WHILE_IN_FOREGROUND === true || isActive === true
        )
    ) {
      this.localNotification.schedule(message, interpolateParams);
    }

  }

  /**
   * @description displays track bad posture prompt if user preferences set
   */
  protected displayTrackPostureModal(message:string, interpolateParams:Object = {}, toast, localNotification):void
  {
    //this.userPreferenceStore.dispatch({type:UserPreferenceAction.USER_PREFERENCE_GET_DATA, payload:{}});
    // check if user preferences set for bad posture alert to come - cross legs prompt
    if(message == AlertConstants.CROSS_LEGS_PROMPT)
    {
      if (this.userPreferenceData.trackBadPosture == true) {
        if (this.userPreferenceData.trackPosture == false) {
          this.userPreferencesProvider.crossLegsModal();
          this.displayMessage(message, interpolateParams, false, true);
        } else {
          this.displayMessage(message, interpolateParams, toast, localNotification);
        }
      }

    }
    // check if user preferences set for bad posture alert
    /*else if(message == AlertConstants.SLOUCHING_PROMPT && this.userPreferenceData.trackPosture == false && this.userPreferenceData.trackBadPosture == true )
    {
      this.userPreferencesProvider.trackPostureModal();
      this.displayMessage(message, interpolateParams, true, false);      
    }*/
  }

}
