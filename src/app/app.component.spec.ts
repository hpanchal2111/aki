import { async, TestBed } from '@angular/core/testing';
import { IonicModule, Platform } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TranslateModule, TranslateService, TranslateLoader, TranslateFakeLoader } from '@ngx-translate/core';

import { MyApp } from './app.component';
import {
  PlatformMock,
  StatusBarMock,
  SplashScreenMock,
  BackgroundModeMock,
  UploadBackgroundProcessProviderMock,
  DevicePollingProviderMock
} from '../../test-config/mocks-ionic';
import { BackgroundMode } from '@ionic-native/background-mode';
import { UploadBackgroundProcessProvider } from '../providers/upload-backgroud-process/upload-backgroud-process.provider';
import { DevicePollingProvider } from '../providers/device-polling/device-polling.provider';


describe('MyApp Component', () => {
  let fixture;
  let component;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MyApp],
      imports: [
        IonicModule.forRoot(MyApp),
        TranslateModule.forRoot({
          loader: { provide: TranslateLoader, useClass: TranslateFakeLoader }
        })
      ],
      providers: [
        { provide: StatusBar, useClass: StatusBarMock },
        { provide: SplashScreen, useClass: SplashScreenMock },
        { provide: Platform, useClass: PlatformMock },
        { provide: BackgroundMode, useClass: BackgroundModeMock },
        { provide: UploadBackgroundProcessProvider, useClass: UploadBackgroundProcessProviderMock },
        { provide: DevicePollingProvider, useClass: DevicePollingProviderMock }
      ]
    })
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyApp);
    component = fixture.componentInstance;
  });

  it('should be created', () => {
    expect(component instanceof MyApp).toBe(true);
  });

  it('root page name is TabsPage', () => {
    console.log(component);
    expect(component.rootPage.name).toBe("TabsPage");
  });

});
