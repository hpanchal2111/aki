import { Injectable, Injector } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';

import 'rxjs/add/operator/do';
import { Observable } from 'rxjs/Observable';
import { Store } from "@ngrx/store";
import { getSessionStateContents } from '../../reducers';
import { SessionState, SessionData } from '../../reducers/session/session.state';
import { ToastProvider } from "../../providers/toast/toast.provider";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  // stores session data
  sessionData: SessionData;  
  public toast: ToastProvider;

  constructor(
    public sessionStore: Store<SessionState>,
    public injector: Injector,
    ) {

    // get user data
    sessionStore.select(getSessionStateContents).subscribe(
      (data: SessionData) => {
        this.sessionData = data;
      },
      error => {
        //this.toast.errorMessage('USER_GET_USER_ERROR');
      }
    );      
  }
  /**
   * @description intercept every http request to add jwt Bearer token if present
   */  
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    //let jwttoken = this.cookieService.get('jwttoken')
    this.toast = this.injector.get(ToastProvider); // get it here within intercept

    if(this.sessionData.bearerToken)
    {
      var jwttoken = 'Bearer ' + this.sessionData.bearerToken;
      request = request.clone({
        setHeaders: {
          // This is where you can use your various tokens
          Authorization: jwttoken
        }
      });
    }  
    return next.handle(request).do((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        // do stuff with response if you want
      }
    }, (err: any) => {
      if (err instanceof HttpErrorResponse) {
        if (err.status === 401 || err.status === 403) {
          this.toast.errorMessage('ERROR_HTTP_REQ_INTERCEPT');
        }
      }
    });
  }
}