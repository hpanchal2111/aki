import { Store } from '@ngrx/store';
import { Component, ViewChild, Input } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TranslateService } from '@ngx-translate/core';
import { LocalizationConstants } from '../constants/localization.constants';
import { BackgroundMode } from '@ionic-native/background-mode';
import { UploadBackgroundProcessProvider } from "../providers/upload-backgroud-process/upload-backgroud-process.provider";
import { DevicePollingProvider } from "../providers/device-polling/device-polling.provider";
import { AuthenticationPage } from '../pages/authentication/authentication';
import { PubNubConstants } from "../constants/pubnub.constants";
import 'rxjs/add/operator/first'
import { Subscription } from "rxjs/Subscription";
import { getRealTimeWebContents, getUserData, getDeviceDiscoveredStateContents } from "../reducers";
import * as RealTimeWebActions from '../reducers/real-time-web/real-time-web.actions'
import { RealTimeWebChannels, RealTimeWebData } from "../reducers/real-time-web/real-time-web.state";
import { ToastProvider } from '../providers/toast/toast.provider';
import { Observable } from 'rxjs/Rx';
import { DeviceConstants } from '../constants/device.constants';
import { DeviceDiscoveredState } from "../reducers/devices/discovered/device-discovered.state";
import { DeviceInterface } from "../interfaces/models/device.interface";
import { LocalNotifications } from "@ionic-native/local-notifications";
import { DashboardConstants } from "../constants/dashboard.constants";
import { UserPreferencesProvider } from "../providers/resource/profile/user-preferences.provider";
import { TabsPage } from '../pages/tabs/tabs';
import { AlertConstants } from '../constants/alert.constants';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = AuthenticationPage;
  pubnub: any;
  subscription: Subscription;
  public counter: number = 0;
  @ViewChild(Nav) navCtrl: Nav;
  /**
   * @description stores scanned devices data
   */
  @Input() devices: DeviceDiscoveredState;

  constructor(platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    translate: TranslateService,
    backgroundMode: BackgroundMode,
    uploadBackgroundProcess: UploadBackgroundProcessProvider,
    devicePollingProvider: DevicePollingProvider,
    public store: Store<any>,
    private toast: ToastProvider,
    public deviceDiscoveredStore: Store<DeviceDiscoveredState>,
    private localNotifications: LocalNotifications,
    public userPreferencesProvider: UserPreferencesProvider
  ) {

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you git pullmight need.
      statusBar.styleDefault();
      splashScreen.hide();

      // set default language translation
      translate.setDefaultLang(LocalizationConstants.DEFAULT_LANGUAGE_TRANSLATION);

      // TODO: add global state translation for app
      translate.use(LocalizationConstants.DEFAULT_LANGUAGE_TRANSLATION);

      devicePollingProvider.scan();
      uploadBackgroundProcess.startQueue();
      devicePollingProvider.startQueue();
      // Enable background mode
      backgroundMode.enable();
      this.subscription = store.select(getUserData)
        .subscribe(
          user => {
            this.getUser(user);
          }
        );
      // Call after 5 second reset confirm exit counter.
      let press_to_exit = Observable.timer(DeviceConstants.PRESS_BACK_TO_EXIT_TIMEOUT);
      platform.registerBackButtonAction(() => {
        if (this.navCtrl.canGoBack()) {

          // if previous page is equal to login or sign up page, exit application
          if (AlertConstants.EXIT_APP_ON_BACK_OF_FF_PAGES.indexOf(this.navCtrl.getPrevious().name) > -1) {
            this.toast.errorMessage('PRESS_AGAIN_EXIT');
            if (this.counter != 0) {
              platform.exitApp();
            }
            this.counter++;
          } else {
            this.navCtrl.pop();
          }
        }
        else {
          if (this.counter == 0) {
            this.counter++;
            this.toast.errorMessage('PRESS_AGAIN_EXIT');
            press_to_exit.subscribe(x => {
              this.counter = 0;
            });
          } else {
            platform.exitApp();
          }
        }

        /**
         * @description beforeunload or when you reload the app disconnet and stop streaming all devices
        */
        window.addEventListener('beforeunload', () => {
          /**
           * @description get scanned devices data
          */
          this.deviceDiscoveredStore.select(getDeviceDiscoveredStateContents)
            .subscribe(
              (data: DeviceDiscoveredState) => {
                this.devices = data;
                Object.keys(this.devices).forEach(data => {
                  let device: DeviceInterface = this.devices[data];
                  device.streamStop();
                  device.onDisconnect();
                });
              },
              error => {
                this.toast.errorMessage('ERROR_DEVICE_NOT_FOUND')
              }
            );
        });
      });

      this.localNotifications.on('click', (notification, state) => {
        if (notification.data && notification.data.redirectTo == DashboardConstants.DASHBOARD) {
          // go to Dashboard tab
          if (this.navCtrl.parent)
            this.navCtrl.parent.select(DashboardConstants.DASHBOARD_INDEX);
          else
            this.navCtrl.push(TabsPage);
          this.userPreferencesProvider.crossLegsModal();
        }
      });

    });
  }

  /**
   * @description
   * @param user user object returned from the userData state from the ngrx store
   */
  protected getUser(user) {
    const userObj = user.userData;
    if (userObj.uuid !== null) {
      if (this.subscription !== null) {
        this.subscription.unsubscribe();
      }
      const realTimeWebData: RealTimeWebData = this.getRealTimeWebState();
      const channel: string = PubNubConstants.ML_CHANNEL_PREFIX + userObj.uuid;
      const uuid: number = userObj.uuid;
      if (userObj.uuid !== realTimeWebData.uuid) {
        console.log('Trigger RTW_INIT');
        this.store.dispatch({
          type: RealTimeWebActions.RTW_INIT, payload: {
            uuid: uuid
          }
        });
        let channels: RealTimeWebChannels = {};
        channels[channel] = {};
        this.store.dispatch({
          type: RealTimeWebActions.RTW_SUBSCRIBE, payload: {
            channels: channels
          }
        });
      }
    }
  }

  /**
   * Get's the state from the store
   * @returns {RealTimeWebData}
   */
  protected getRealTimeWebState(): RealTimeWebData {
    let realTimeWebData: RealTimeWebData;
    let subscription: Subscription = this.store.select(getRealTimeWebContents).subscribe(s => realTimeWebData = s);
    subscription.unsubscribe();
    return realTimeWebData;
  }
}
