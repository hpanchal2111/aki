import { UserProvider } from '../providers/resource/profile/user.provider';
import { DashboardProvider } from '../providers/resource/machine-learning/dashboard.provider';
import { OnboardingPage } from '../pages/onboarding/onboarding';
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { BLE } from '@ionic-native/ble';
import { StoreModule } from '@ngrx/store';
import { BackgroundMode } from '@ionic-native/background-mode';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StorageSyncEffects } from 'ngrx-store-ionic-storage';
import { IonicStorageModule } from '@ionic/storage';
import { EffectsModule } from '@ngrx/effects';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TooltipsModule } from 'ionic-tooltips';
import { CookieService } from 'ngx-cookie-service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './auth/token.interceptor';

// Pages
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { DevicePage } from '../pages/device/device';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { SettingsPage } from '../pages/settings/settings';
import { AuthenticationPage } from '../pages/authentication/authentication';
import { SignupPage } from '../pages/signup/signup';
import { LoginPage } from '../pages/login/login';
import { PairDevicePage } from '../pages/pair-device/pair-device';
import { TroublePairDevicePage } from '../pages/trouble-pair-device/trouble-pair-device';
import { PairDeviceSuccessPage } from '../pages/pair-device-success/pair-device-success';
import { TestWalkPage } from '../pages/test-walk/test-walk';
import { TrackPostureModalPage } from '../pages/track-posture-modal/track-posture-modal';
import { CrossLegsModalPage } from '../pages/cross-legs-modal/cross-legs-modal';

// Components
import { MyApp } from './app.component';
import { DeviceListComponent } from '../components/device-list/device-list';
import { BarChartComponent } from '../components/charts/bar-chart/bar-chart';
import { LineChartComponent } from '../components/charts/line-chart/line-chart';
import { PieChartComponent } from '../components/charts/pie-chart/pie-chart';
import { PolarChartComponent } from '../components/charts/polar-chart/polar-chart';
import { SecretButtonComponent } from '../components/secret-button/secret-button';

// Providers
import { SweetSpotProvider } from '../providers/sweet-spot/sweet-spot.provider';
import { BufferArrayProvider } from '../providers/buffer-array/buffer-array';
import { ToastProvider } from '../providers/toast/toast.provider';
import { AuthProvider } from '../providers/auth/auth';
import { SweetSpotArrayBufferProvider } from '../providers/sweet-spot-array-buffer/sweet-spot-array-buffer.provider';
import { UploadBackgroundProcessProvider } from '../providers/upload-backgroud-process/upload-backgroud-process.provider';
import { PubNubAngular } from 'pubnub-angular2';
import { PubNubProvider } from '../providers/pub-nub/pub-nub';
import { DataProvider } from '../providers/resource/machine-learning/data.provider';
import { MachineLearningApiProvider } from '../providers/api/machine-learning/machine-learning-api.provider';
import { DevicePollingProvider } from '../providers/device-polling/device-polling.provider';
import { SessionApiProvider } from '../providers/api/session/session-api.provider';

// Reducers

import { metaReducers, reducers } from '../reducers';

// Effects
import { DashboardEffects } from '../effects/dashboard.effects';
import { UserEffects } from '../effects/user.effects';
import { ProfileProvider } from '../providers/api/profile/profile.provider';
import { AlertEffects } from "../effects/alert.effects";
import { UserPreferencesProvider } from "../providers/resource/profile/user-preferences.provider";
import { UserPreferencesEffects } from "../effects/user-preferences.effects";
import { LocalNotificationProvider } from "../providers/local-notification/local-notification.provider";
import { LocalNotifications } from "@ionic-native/local-notifications";
import { RealTimeWebEffects } from "../effects/real-time-web.effects";
import { ViewReportPage } from '../pages/view-report/view-report';
import { ShareReportModalPage } from '../pages/share-report-modal/share-report-modal';

// change default translation folder
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    TrackPostureModalPage,
    DevicePage,
    AuthenticationPage,
    LoginPage,
    SignupPage,
    TestWalkPage,
    SettingsPage,
    PairDevicePage,
    TroublePairDevicePage,
    PairDeviceSuccessPage,
    OnboardingPage,
    DashboardPage,
    CrossLegsModalPage,
    ViewReportPage,
    ShareReportModalPage,
    BarChartComponent,
    LineChartComponent,
    PieChartComponent,
    PolarChartComponent,
    DeviceListComponent,
    SecretButtonComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    TooltipsModule,
    IonicStorageModule.forRoot(),
    EffectsModule.forRoot([
      StorageSyncEffects,
      DashboardEffects,
      UserEffects,
      AlertEffects,
      UserPreferencesEffects,
      RealTimeWebEffects
    ]),
    StoreModule.forRoot(reducers, {
      metaReducers,
      initialState: {
        hydrated: false
      }
    }),
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    TrackPostureModalPage,
    DevicePage,
    AuthenticationPage,
    LoginPage,
    SignupPage,
    TestWalkPage,
    DashboardPage,
    SettingsPage,
    PairDevicePage,
    TroublePairDevicePage,
    PairDeviceSuccessPage,
    OnboardingPage,
    CrossLegsModalPage,
    ViewReportPage,
    ShareReportModalPage,
  ],
  exports: [TranslateModule],
  providers: [
    StatusBar,
    SplashScreen,
    BackgroundMode,
    BLE,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    SweetSpotProvider,
    BufferArrayProvider,
    ToastProvider,
    HttpClient,
    SweetSpotArrayBufferProvider,
    HttpClient,
    AuthProvider,
    UploadBackgroundProcessProvider,
    PubNubAngular,
    PubNubProvider,
    DashboardProvider,
    DataProvider,
    UserProvider,
    MachineLearningApiProvider,
    DevicePollingProvider,
    ProfileProvider,
    UserPreferencesProvider,
    LocalNotificationProvider,
    LocalNotifications,
    SessionApiProvider,
    CookieService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }    
]
})
export class AppModule { }
