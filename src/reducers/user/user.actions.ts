/**
 * Actions related to dashboard page
 * @type {string}
 */

export const USER_GET_USER = 'USER_GET_USER';
export const USER_GET_USER_ERROR = 'USER_GET_USER_ERROR';
export const USER_GET_USER_ERROR_HANDLED = 'USER_GET_USER_ERROR_HANDLED';
export const USER_SET_DATA = 'USER_SET_DATA';
export const USER_SET_DATA_HANDLED = 'USER_SET_DATA_HANDLED';
export const USER_SET_DATA_ERROR = 'USER_SET_DATA_ERROR';
export const USER_GET_DATA_HANDLED = 'USER_GET_DATA_HANDLED';