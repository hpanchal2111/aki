
/**
 * @description Interface for UserData
 */
export interface UserData {
  name: string;
  age: number;
  weight: number;
  height: number;
  uuid: number;
  testWalkSteps: number;
  testWalkSuccess: boolean;
  startTestWalk?: boolean;
  loaded: boolean;
}

export interface UserDataState {
  userData: UserData;

}

/**
* @description Initial state for UserData
*/
export const InitialUserDataState: UserDataState = {
  userData: {
    name: null,
    age: null,
    weight: null,
    height: null,
    uuid: null,
    testWalkSteps: null,
    testWalkSuccess: null,
    startTestWalk: null,
    loaded: null,
  },
};
