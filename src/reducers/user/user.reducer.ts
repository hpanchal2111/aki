import { InitialUserDataState, UserDataState, UserData } from "./user.state";
import { ActionWithPayload } from "../../interfaces/actions/action-with-payload.interface";
import * as UserActions from './user.actions'

/**
 * @description Reducer for userData
 * @param state
 * @param action
 */
export function UserReducer(state: UserDataState = InitialUserDataState, action: ActionWithPayload<UserData>): UserDataState {
    let newState:UserDataState = {
      userData: {...state.userData},
    };
    switch (action.type) {
      case UserActions.USER_GET_USER:
        return newState = InitialUserDataState;
      case UserActions.USER_GET_DATA_HANDLED:
        newState.userData = action.payload;
        return newState;
      case UserActions.USER_SET_DATA:
        newState.userData = action.payload;
        return newState;
      default:
        return newState;
    }
}
