/**
 * @author William Tempest Wright Ferrer
 */
/**
 * @description Actions related to storing data that is gathered from the device
 * @type {string}
 */
export const DEVICE_STORE_DATA = 'DEVICE_STORE_DATA';
export const DEVICE_REMOVE_DATA = 'DEVICE_REMOVE_DATA';
export const DEVICE_QUEUE_DATA = 'DEVICE_QUEUE_DATA';
export const DEVICE_REMOVE_QUEUE_DATA = 'DEVICE_REMOVE_QUEUE_DATA';


