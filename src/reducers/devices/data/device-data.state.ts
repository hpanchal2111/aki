/**
 * @author William Tempest Wright Ferrer
 */
/**
 * @description Interface and initial value constant for the device data store.
 * Data is stored related to where the device is attached on the users body
 */

export interface DeviceData {
  location?:string,
  timestamp?:number,
  deviceId?:string,
  machineLearningSessionId?:string,
  ACC_X?:number,
  ACC_Y?:number,
  ACC_Z?:number,
  GYR_X?:number,
  GYR_Y?:number,
  GYR_Z?:number
}

export interface DeviceTimeData {
  [index: string]:DeviceData
}

export interface DeviceDataInside {
  queued:DeviceTimeData,
  collected:DeviceTimeData
}

export interface DeviceDataState {
  deviceData : DeviceDataInside
}

export const InitialDeviceDataState: DeviceDataState = {
  deviceData : {
    queued: {},
    collected: {}
  }
};
