/**
 * @author William Tempest Wright Ferrer
 */
import * as DeviceAction from './device-data.actions'
import {DeviceDataState, DeviceTimeData, InitialDeviceDataState} from "./device-data.state";
import {DeviceDataAction} from "../../../interfaces/actions/device-data-action.interface";
import { DeviceConstants } from '../../../constants/device.constants';

/**
 * @description This reducer stores data that is sent from the device about movement data it has collected.
 * @param {DeviceDataState} state
 * @param {DeviceDataAction<DeviceTimeData>} action
 * @returns {DeviceDataState}
 * @constructor
 */
export function DeviceDataReducer(state: DeviceDataState = InitialDeviceDataState, action: DeviceDataAction<DeviceTimeData>): DeviceDataState {
  //TODO: Replace with https://facebook.github.io/immutable-js/
  let newState = {
    deviceData: {
      queued: { ...state.deviceData.queued }, collected: { ...state.deviceData.collected }
    }
  };
  //let newState = DeepCopy.deepCopy(state);
  switch (action.type) {
    // Stores the keys and values from the action to the collected object in the state
    case DeviceAction.DEVICE_STORE_DATA:

      // Stop storing data if over the max set value on DeviceConstants.MAX_NGRX_DEVICE_STORE_DATA
      if (Object.keys(newState.deviceData.collected).length < DeviceConstants.MAX_NGRX_DEVICE_STORE_DATA) {
        newState.deviceData.collected = Object.assign(newState.deviceData.collected, action.payload);
      }
      return newState;
    case DeviceAction.DEVICE_REMOVE_DATA:
      // Removes the keys passed from the collected object in the state
      for (let key in action.payload) {
        delete newState.deviceData.collected[key];
      }
      console.log('collected data after removal', Object.keys(newState.deviceData.collected).length);
      return newState;
    case DeviceAction.DEVICE_QUEUE_DATA:
      // Moves the keys passed from the collected object in the state to the queued object in the state
      for (let key in action.payload) {
        newState.deviceData.queued[key] = newState.deviceData.collected[key];
        delete newState.deviceData.collected[key];
      }
      console.log('queued data after queue', Object.keys(newState.deviceData.queued).length);
      console.log('collected data after queue', Object.keys(newState.deviceData.collected).length);
      return newState;
    case DeviceAction.DEVICE_REMOVE_QUEUE_DATA:
      // Removes the keys passed from the queued object in the state
      for (let key in action.payload) {
        delete newState.deviceData.queued[key];
      }
      console.log('queued data after queue clear', Object.keys(newState.deviceData.queued).length);
      return newState;
    default:
      return newState;
  }
}
