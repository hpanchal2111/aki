/**
 * @author William Tempest Wright Ferrer
 */
import * as DeviceAction from './device-discovered.actions'
import { DeviceDiscoveredState, InitialDeviceDiscoveredState } from "./device-discovered.state";
import { ActionWithPayload } from "../../../interfaces/actions/action-with-payload.interface";
import { DeviceInterface } from "../../../interfaces/models/device.interface";

/**
 * @description This reducer stores data to the store that relates to that devices are currently connected to the app.
 * @param {DeviceDiscoveredState} state
 * @param {ActionWithPayload<DeviceInterface>} action
 * @returns {DeviceDiscoveredState}
 * @constructor
 */
export function DeviceDiscoveredReducer(state: DeviceDiscoveredState = InitialDeviceDiscoveredState, action: ActionWithPayload<DeviceInterface>): DeviceDiscoveredState {
  let newState:DeviceDiscoveredState = {
    deviceDiscovered: {...state.deviceDiscovered}
  };
  switch (action.type) {
    case DeviceAction.DEVICE_DISCOVERED:
      newState.deviceDiscovered[action.payload.getPeripheralId()] = action.payload;
      return newState;
    case DeviceAction.DEVICE_DISCONNECTED:
      delete newState.deviceDiscovered[action.payload.getPeripheralId()];
      return newState;
    case DeviceAction.DEVICE_UPDATED:
      return newState;
    default:
      return newState;
  }
}
