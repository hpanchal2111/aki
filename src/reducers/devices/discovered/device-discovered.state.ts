/**
 * @author William Tempest Wright Ferrer
 */
import {DeviceInterface} from "../../../interfaces/models/device.interface";

/**
 * @description Interface and initial value constant for the device connected store.
 */

export interface DeviceDiscoveredInfo {
  [index: string]:DeviceInterface
}

export interface DeviceDiscoveredState {
  deviceDiscovered:DeviceDiscoveredInfo
}

export const InitialDeviceDiscoveredState: DeviceDiscoveredState = {
  deviceDiscovered: {}
};
