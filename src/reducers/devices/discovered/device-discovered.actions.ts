/**
 * @author William Tempest Wright Ferrer
 */

/**
 * @description Actions related to when a device is connected or disconnected
 * @type {string}
 */
export const DEVICE_DISCOVERED = 'DEVICE_DISCOVERED';
export const DEVICE_DISCONNECTED = 'DEVICE_DISCONNECTED';
export const DEVICE_UPDATED = 'DEVICE_UPDATED';
