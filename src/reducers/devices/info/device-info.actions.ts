/**
 * Actions related to when the data about a device is updated
 * @type {string}
 */

export const DEVICE_UPDATED = 'DEVICE_UPDATED';
