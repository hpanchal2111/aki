

import * as DeviceAction from '../discovered/device-discovered.actions'
import * as DeviceInfoAction from './device-info.actions'
import {DeviceInfo, DeviceInfoState, InitialDeviceInfoState} from "./device-info.state";
import {ActionWithPayload} from "../../../interfaces/actions/action-with-payload.interface";
import {DeepCopy} from "../../../helper/deep-copy.helper";
import {DeviceInterface} from "../../../interfaces/models/device.interface";
import {DeviceConstants} from "../../../constants/device.constants";


function storeDeviceInfo(newState: DeviceInfoState, action: ActionWithPayload<DeviceInterface>):DeviceInfoState {
  //TODO: Replace with https://facebook.github.io/immutable-js/

  let currentTime:number = new Date().getTime();
  let peripheralId:string = action.payload.getPeripheralId();
  let saveUntil:number = currentTime - DeviceConstants.SAVE_DEVICE_INFO_TIME;
  const newInfo:DeviceInfo = {
    id:peripheralId,
    timestamp:currentTime,
    battery:action.payload.battery,
    dataToCapture:action.payload.dataToCapture,
    location:action.payload.location,
    frequency:action.payload.frequency,
    isConnected:action.payload.isConnected,
    assignedBits:action.payload.assignedBits,
    isStreaming:action.payload.isStreaming,
    rssi:action.payload.rssi,
    isPolling:action.payload.isPolling,
  };
  console.log('device rssi', newInfo.rssi);
  if (newState.deviceInfo.hasOwnProperty(peripheralId) === false) {
    newState.deviceInfo[peripheralId] = [];
  }
  newState.deviceInfo[peripheralId].push(newInfo);
  if (newState.deviceInfo.hasOwnProperty(peripheralId) !== false) {
    newState.deviceInfo[peripheralId] = newState.deviceInfo[peripheralId].filter(data => data.timestamp > saveUntil)
  }
  return newState;
}
/**
 * This reducer takes info about the device as it changes and it adds to a store
 * @param {DeviceInfoState} state
 * @param {ActionWithPayload<DeviceInterface>} action
 * @returns {DeviceInfoState}
 * @constructor
 */
export function DeviceInfoReducer(state: DeviceInfoState = InitialDeviceInfoState, action: ActionWithPayload<DeviceInterface>): DeviceInfoState {
  let newState:DeviceInfoState = DeepCopy.deepCopy(state);
  switch (action.type) {
    case DeviceAction.DEVICE_DISCOVERED:
      newState = storeDeviceInfo(newState, action);
      return newState;
    case DeviceInfoAction.DEVICE_UPDATED:
      newState = storeDeviceInfo(newState, action);
      return newState;
    case DeviceAction.DEVICE_DISCONNECTED:
      let peripheralId:string = action.payload.getPeripheralId();
      delete newState.deviceInfo[peripheralId];
      return newState;
    default:
      return newState;
  }


}
