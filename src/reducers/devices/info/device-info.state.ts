
import {LocationType} from "../../../types/location.type";
import {AssignBytesType} from "../../../types/assign-bytes.type";


/**
 * Interface and initial value constant for the device info store.
 */

export interface DeviceInfo {
  id:string;
  timestamp:number,
  battery:string;
  dataToCapture:Array<string>;
  location:LocationType;
  frequency:number;
  isConnected:boolean;
  assignedBits:AssignBytesType;
  isStreaming:boolean;
  rssi:number;
  isPolling:boolean;
}

export interface DeviceInfoList {
  [key:string]:Array<DeviceInfo>;
}

export interface DeviceInfoState {
  deviceInfo:DeviceInfoList;
}

export const InitialDeviceInfoState: DeviceInfoState = {
  deviceInfo: {}
};

