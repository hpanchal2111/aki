/**
 * @author William Tempest Wright Ferrer
 */

/**
 * @description Actions related to storing a machine learning session
 * @type {string}
 */
export const SET_MACHINE_LEARNING_SESSION = 'SET_MACHINE_LEARNING_SESSION';

