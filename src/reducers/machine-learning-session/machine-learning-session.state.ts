/**
 * @author William Tempest Wright Ferrer
 */

/**
 * @description Interface and initial value constant for the device connected store.
 */
export interface MachineLearningSessionState {
  machineLearningSessionId:string
}

export const InitialMachineLearningSessionState: MachineLearningSessionState = {
  machineLearningSessionId: ''
};
