/**
 * @author William Tempest Wright Ferrer
 */
import * as Actions from './machine-learning-session.actions'
import {
  InitialMachineLearningSessionState,
  MachineLearningSessionState
} from "./machine-learning-session.state";
import { ActionWithPayload } from "../../interfaces/actions/action-with-payload.interface";

/**
 * @description This reducer stores data to the store that relates to that devices are currently connected to the app.
 * @param {DeviceDiscoveredState} state
 * @param {ActionWithPayload<string>} action
 * @returns {DeviceDiscoveredState}
 * @constructor
 */
export function MachineLearningSessionReducer(state: MachineLearningSessionState = InitialMachineLearningSessionState, action: ActionWithPayload<string>): MachineLearningSessionState {
  let newState: MachineLearningSessionState = {
    'machineLearningSessionId':''
  };

  switch (action.type) {
    case Actions.SET_MACHINE_LEARNING_SESSION:
      newState['machineLearningSessionId'] = action.payload;
      return newState;
    default:
      return newState;
  }
}
