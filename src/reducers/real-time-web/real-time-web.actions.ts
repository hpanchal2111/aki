/**
 * Actions related to real time web state
 * @type {string}
 */

export const RTW_INIT = 'RTW_INIT';
export const RTW_SUBSCRIBE = 'RTW_SUBSCRIBE';
export const RTW_UNSUBSCRIBE = 'RTW_UNSUBSCRIBE';
export const RTW_INIT_ERROR = 'RTW_INIT_ERROR';
export const RTW_SUBSCRIBE_ERROR = 'RTW_SUBSCRIBE_ERROR';
export const RTW_UNSUBSCRIBE_ERROR = 'RTW_UNSUBSCRIBE_ERROR';
export const RTW_INIT_ERROR_HANDLED = 'RTW_INIT_ERROR_HANDLED';
export const RTW_SUBSCRIBE_ERROR_HANDLED = 'RTW_SUBSCRIBE_ERROR_HANDLED';
export const RTW_UNSUBSCRIBE_ERROR_HANDLED = 'RTW_UNSUBSCRIBE_ERROR_HANDLED';
export const RTW_INIT_HANDLED = 'RTW_INIT_HANDLED';
export const RTW_SUBSCRIBE_HANDLED = 'RTW_SUBSCRIBE_HANDLED';
export const RTW_UNSUBSCRIBE_HANDLED = 'RTW_UNSUBSCRIBE_HANDLED';

