
export interface RealTimeWebChannelSettings {
    triggerEvents?: boolean,
    withPresence?: boolean
}
export interface RealTimeWebChannels {
    [indexof: string]:RealTimeWebChannelSettings
}
/**
 * @description Interface for UserPreferenceData
 */
export interface RealTimeWebData {
    uuid?:number,
    channels?:RealTimeWebChannels
}

export interface RealTimeWebState {
    realTimeWeb: RealTimeWebData;
}

/**
 * @description Initial state for UserPreferenceData
 */ 
export const InitialRealTimeWebState: RealTimeWebState = {
    realTimeWeb: {
        uuid:null,
        channels: {}
    }
};
