import {
    RealTimeWebState, InitialRealTimeWebState, RealTimeWebData
} from "./real-time-web.state";
import { ActionWithPayload } from "../../interfaces/actions/action-with-payload.interface";
import * as RealTimeWebActions from './real-time-web.actions'

/**
 * @description Reducer for userPreferenceData
 * @param state
 * @param action
 */
export function RealTimeWebReducer(state: RealTimeWebState = InitialRealTimeWebState, action: ActionWithPayload<RealTimeWebData>): RealTimeWebState {
    let newState:RealTimeWebState = {
      realTimeWeb: {
            uuid:state.realTimeWeb.uuid,
            channels:{...state.realTimeWeb.channels}
        }
    };
    switch (action.type) {
      case RealTimeWebActions.RTW_INIT_HANDLED:
          console.log('RTW_INIT_HANDLED');
          newState.realTimeWeb.uuid = action.payload.uuid;
          return newState;
        case RealTimeWebActions.RTW_SUBSCRIBE_HANDLED:
            newState.realTimeWeb.channels = Object.assign(newState.realTimeWeb.channels, action.payload.channels);
            return newState;
        case RealTimeWebActions.RTW_UNSUBSCRIBE_HANDLED:
            for (let key in newState.realTimeWeb.channels) {
                if (newState.realTimeWeb.channels.hasOwnProperty(key)) {
                    delete newState.realTimeWeb.channels[key];
                }
            }
            return newState;
        default:
          return newState;
    }
}
