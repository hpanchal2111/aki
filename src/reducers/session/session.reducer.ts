import * as Actions from './session.actions'
import {
  InitialSessionState,
  SessionState,
  SessionData
} from "./session.state";
import { ActionWithPayload } from "../../interfaces/actions/action-with-payload.interface";

/**
 * @description This reducer stores session data 
 * @param {DeviceDiscoveredState} state
 * @param {ActionWithPayload<string>} action
 * @returns {DeviceDiscoveredState}
 * @constructor
 */
export function SessionReducer(state: SessionState = InitialSessionState, action: ActionWithPayload<SessionData>): SessionState {
  let newState: SessionState = {
    sessionData: {...state.sessionData},
  };

  switch (action.type) {
    case Actions.SET_SESSION:
      newState.sessionData = action.payload;
      return newState;
    default:
      return newState;
  }
}
