/**
 * @description Actions related to storing a session
 * @type {string}
 */
export const SET_SESSION = 'SET_SESSION';

