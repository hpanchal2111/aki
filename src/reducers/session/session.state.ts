/**
 * @description Interface and initial value constant for the session store.
 */
 export interface SessionData {
  bearerToken: string;
  ReceivedAtTimeStamp: number;
}

export interface SessionState {
  sessionData: SessionData;
}

export const InitialSessionState: SessionState = {
  sessionData:{
	  bearerToken: null,
	  ReceivedAtTimeStamp: null
  }
};
