
export interface AppState {
  deviceDiscovered: any;
  deviceData: any;
  machineLearningSessionId: any;
  deviceInfo: any;
  hydrated: any;
  dashboardData: any;
  userPreferenceData: any;
  userData: any;
  alert: any;
  realTimeWeb:any;
  sessionData: any;
}
