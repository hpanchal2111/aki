
/**
 * @description Interface for UserPreferenceData
 */
export interface UserPreferenceData {
    trackPosture?: boolean;
    trackBadPosture?: boolean;
    isTrackPromptFirstTime?: boolean;
    testWalkSuccess?:boolean
}

export interface UserPreferenceDataState {
    userPreferenceData: UserPreferenceData;
}

/**
 * @description Initial state for UserPreferenceData
 */ 
export const InitialUserPreferenceDataState: UserPreferenceDataState = {
    userPreferenceData: {
        trackPosture: true,
        trackBadPosture: true,
        isTrackPromptFirstTime: true,
        testWalkSuccess:false
    }
};
