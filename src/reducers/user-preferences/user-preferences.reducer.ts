import { InitialUserPreferenceDataState, UserPreferenceDataState, UserPreferenceData } from "./user-preferences.state";
import { ActionWithPayload } from "../../interfaces/actions/action-with-payload.interface";
import * as UserPreferenceAction from './user-preferences.actions'

/**
 * @description Reducer for userPreferenceData
 * @param state
 * @param action
 */
export function UserPreferenceReducer(state: UserPreferenceDataState = InitialUserPreferenceDataState, action: ActionWithPayload<UserPreferenceData>): UserPreferenceDataState {
    let newState:UserPreferenceDataState = {
      userPreferenceData: {...state.userPreferenceData}
    };
    switch (action.type) {
        case UserPreferenceAction.USER_PREFERENCE_SET_DATA_HANDLED:
          newState.userPreferenceData = action.payload;
          return newState;
        default:
          return newState;
    }
}
