
/**
 * @description Interface for UserData
 */
export interface AlertData {
  type: string;
  message: string;
  interpolateParams?: Object,
  toast?:boolean,
  localNotification?:boolean
}

export interface AlertState {
  alert: Array<AlertData>;
}

/**
* @description Initial state for UserData
*/
export const InitialAlertState: AlertState = {
  alert: []
};
