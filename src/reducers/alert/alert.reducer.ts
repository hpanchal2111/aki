import { AlertState, InitialAlertState, AlertData} from "./alert.state";
import { ActionWithPayload } from "../../interfaces/actions/action-with-payload.interface";
import * as AlertActions from './alert.actions'
import {AlertConstants} from "../../constants/alert.constants";

/**
 * @description Reducer for userData
 * @param state
 * @param action
 */
export function AlertReducer(state: AlertState = InitialAlertState, action: ActionWithPayload<Array<AlertData>>): AlertState {
    let newState:AlertState = {
      alert: [...state.alert]
    };
    switch (action.type) {
      case AlertActions.ADD_ALERT_HANDLED:
        newState.alert.push(action.payload[0]);
        if (newState.alert.length > AlertConstants.MAX) {
          newState.alert.shift();
        }
        return newState;
      case AlertActions.ADD_ALERTS:
        newState.alert.push(...action.payload);
        return newState;
      default:
        return newState;
    }
}
