/**
 * Actions related to dashboard page
 * @type {string}
 */
export const ADD_ALERT_HANDLED = 'ADD_ALERT_HANDLED';
export const ADD_ALERT_RTW = 'ADD_ALERT_RTW';
export const ADD_ALERTS = 'ADD_ALERTS';
