/**
 * @author Marat.Bokov
 * @description Reducer combiner into a single store
 */

import {
  MetaReducer,
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
} from '@ngrx/store';
import { storageSync } from 'ngrx-store-ionic-storage';
import {
  AppState
} from './reducer.interfaces';
import { DeviceDiscoveredState } from "./devices/discovered/device-discovered.state";
import { DeviceDataInside, DeviceDataState, DeviceTimeData } from './devices/data/device-data.state';
import { MachineLearningSessionState } from './machine-learning-session/machine-learning-session.state';
import { SessionState } from './session/session.state';

import * as fromDiscovered from './devices/discovered/device-discovered.reducer';
import * as fromDeviceData from './devices/data/device-data.reducer';
import * as fromMLS from './machine-learning-session/machine-learning-session.reducer';
import * as fromSession from './session/session.reducer';
import * as fromDeviceInfo from './devices/info/device-info.reducer';
import * as fromDashboard from './dashboard/dashboard.reducer';
import * as fromUserPreference from './user-preferences/user-preferences.reducer';
import * as fromUser from './user/user.reducer';
import * as fromAlert from './alert/alert.reducer';
import * as fromRealTimeWeb from './real-time-web/real-time-web.reducer';

import { DeviceConstants } from "../constants/device.constants";
import { DeviceInfo, DeviceInfoList, DeviceInfoState } from "./devices/info/device-info.state";
import { DashboardDataState } from './dashboard/dashboard.state';
import { UserPreferenceDataState } from './user-preferences/user-preferences.state';
import * as fromHydrated from './hydrated.reducer';
import { UserDataState } from "./user/user.state";
import { AlertState } from "./alert/alert.state";
import { RealTimeWebState } from "./real-time-web/real-time-web.state";

export function stateLogger(reducer: ActionReducer<AppState>): ActionReducer<AppState> {
  return (state: AppState, action: any): AppState => {
    // console.warn('Action: ', action);
    // console.warn('STATE: ', state);
    return reducer(state, action);
  }
}

export const reducers: ActionReducerMap<AppState> = {
  deviceDiscovered: fromDiscovered.DeviceDiscoveredReducer,
  deviceData: fromDeviceData.DeviceDataReducer,
  machineLearningSessionId: fromMLS.MachineLearningSessionReducer,
  sessionData: fromSession.SessionReducer,
  deviceInfo: fromDeviceInfo.DeviceInfoReducer,
  hydrated: fromHydrated.reducer,
  dashboardData: fromDashboard.DashboardReducer,
  userPreferenceData: fromUserPreference.UserPreferenceReducer,
  userData: fromUser.UserReducer,
  alert: fromAlert.AlertReducer,
  realTimeWeb:fromRealTimeWeb.RealTimeWebReducer
};

// TODO: Re-enable for MVP
export const storageSyncReducer = storageSync({
  keys: [
    //'deviceData',
    //'dashboardData',
    //'userPreferenceData',
    //'userData',
    //'alert'
  ],
  hydratedStateKey: 'hydrated',
  ignoreActions: [        // Don't sync when these actions occur

  ],
  onSyncError: onSyncError
});

export function storageMetaReducer(reducer: ActionReducer<AppState>): ActionReducer<AppState> {
  return storageSyncReducer(reducer);
}

export const metaReducers: MetaReducer<any, any>[] = [stateLogger, storageMetaReducer];

// Feature Selectors
export const getDeviceDiscoveredState = createFeatureSelector<DeviceDiscoveredState>('deviceDiscovered');
export const getDeviceDataState = createFeatureSelector<DeviceDataState>('deviceData');
export const getMLSState = createFeatureSelector<MachineLearningSessionState>('machineLearningSessionId');
export const getSessionData = createFeatureSelector<SessionState>('sessionData');
export const getDeviceInfo = createFeatureSelector<DeviceInfoState>('deviceInfo');
export const getDashboardData = createFeatureSelector<DashboardDataState>('dashboardData');
export const getUserPreferenceData = createFeatureSelector<UserPreferenceDataState>('userPreferenceData');
export const getUserData = createFeatureSelector<UserDataState>('userData');
export const getAlertState = createFeatureSelector<AlertState>('alert');
export const getRealTimeWebState = createFeatureSelector<RealTimeWebState>('realTimeWeb');

// Contents of a feature
export const getDeviceDiscoveredStateContents = createSelector(
  getDeviceDiscoveredState,
  (state: AppState) => {
    return state.deviceDiscovered;
  }
);

export function onSyncError(err) {
  console.error(err);
}

export const getDeviceDataStateContents = createSelector(
  getDeviceDataState,
  (state: AppState) => {
    return state.deviceData;
  }
);

export const getMachineLearningSessionStateContents = createSelector(
  getMLSState,
  (state: AppState) => {
    return state.machineLearningSessionId;
  }
);

export const getSessionStateContents = createSelector(
  getSessionData,
  (state: AppState) => {
    return state.sessionData;
  }
);

export const getDeviceInfoStateContents = createSelector(
  getDeviceInfo,
  (state: AppState) => {
    return state.deviceInfo;
  }
);

/**
 * @description Custom selector for dashboardData
 */
export const getDashboardDataStateContents = createSelector(
  getDashboardData,
  (state: AppState) => {
    return state.dashboardData;
  }
);

/**
 * @description Custom selector for userPreferenceData
 */
export const getUserPreferenceDataStateContents = createSelector(
  getUserPreferenceData,
  (state: AppState) => {
    return state.userPreferenceData;
  }
);

/**
 * @description Custom selector for userData
 */
export const getUserDataStateContents = createSelector(
  getUserData,
  (state: AppState) => {
    return state.userData;
  }
);

/**
 * @description Custom selector for alert
 */
export const getAlertContents = createSelector(
  getAlertState,
  (state: AppState) => {
    return state.alert;
  }
);

/**
 * @description Custom selector for alert
 */
export const getRealTimeWebContents = createSelector(
  getRealTimeWebState,
  (state: AppState) => {
    return state.realTimeWeb;
  }
);

// Device Data Selectors
export const getDeviceDataStateHeadCollection = createSelector(
  getDeviceDataStateContents,
  (state: DeviceDataInside) => {
    let slice: DeviceTimeData = {};
    let count: number = 0;
    for (let key in state.collected) {
      slice[key] = state.collected[key];
      count++;
      if (count >= DeviceConstants.QUEUE_COUNT) {
        break;
      }
    }
    return slice;
  }
);

export const getDeviceDataStateQueued = createSelector(
  getDeviceDataStateContents,
  (state: DeviceDataInside) => state.queued
);

// Device Info Selectors
export const getDevicesLostConnection = createSelector(
  getDeviceInfoStateContents,
  (devicesInfo: DeviceInfoList) => {
    let currentTime: number = new Date().getTime();
    let deviceDiscoveredIds: Array<string> = [];
    let timeout: number = currentTime - DeviceConstants.RSSI_TIMEOUT;
    for (let key in devicesInfo) {
      let history: Array<DeviceInfo> = devicesInfo[key];
      let subject: DeviceInfo = history[history.length - 1];
      // If the last time the device reported in was longer ago than the timeout, then we are having trouble reaching it
      if (subject.timestamp <= timeout) {
        deviceDiscoveredIds.push(key);
        break;
      }
      for (let n: number = history.length - 1; n > 0; n--) {
        subject = history[n];
        // If it's over the threshold on this capture, then we are good
        if (subject.rssi > DeviceConstants.RSSI_THRESHOLD_DISCONNECT) {
          break;
        }
        // If it didn't break above, we check if the timestamp is long enough ago that we consider it disconnected
        if (subject.timestamp <= timeout) {
          deviceDiscoveredIds.push(key);
          break;
        }
      }
    }
    return deviceDiscoveredIds;
  }
);

export const getDevicesToPair = createSelector(
  getDeviceInfoStateContents,
  (devicesInfo: DeviceInfoList) => {
    let currentTime: number = new Date().getTime();
    let devicesToPair: Array<string> = [];
    for (let key in devicesInfo) {
      let history: Array<DeviceInfo> = devicesInfo[key];
      for (let n: number = history.length-1; n > 0; n--) {
        let subject: DeviceInfo = history[n];
        // If it's under the threshold on this capture, then it's not close enough to pair so break
        if (subject.rssi <= DeviceConstants.RSSI_THRESHOLD_PAIR) {
          break;
        }
        // If it didn't break above, we check if the timestamp is long enough ago that we consider it has been on the screen for enough time to pair
        if (subject.timestamp <= currentTime - DeviceConstants.TIME_TO_PAIR) {
          devicesToPair.push(key);
          break;
        }
      }
    }
    return devicesToPair;
  }
);

// Device rssi is good enough to pair
export const getDevicesSignalToPairUpdate = createSelector(
  getDeviceInfoStateContents,
  (devicesInfo: DeviceInfoList) => {
    let deviceRssiHighToPair: boolean = false;  
    for (let key in devicesInfo) {
      let history: Array<DeviceInfo> = devicesInfo[key];
      let subject: DeviceInfo = history[history.length - 1];
      for (let n: number = history.length - 1; n > 0; n--) {
        subject = history[n];
        // If it's over the threshold on this capture, then we are good
        if (subject.rssi > DeviceConstants.RSSI_THRESHOLD_DISCONNECT) {
          deviceRssiHighToPair = true; 
          break;
        }
      }
    }
    return deviceRssiHighToPair;
  }
);