import { InitialDashboardDataState, DashboardDataState, DashboardData } from "./dashboard.state";
import { ActionWithPayload } from "../../interfaces/actions/action-with-payload.interface";
import * as DashboardAction from './dashboard.actions'

/**
 * @description Reducer for dashboardData
 * @param state
 * @param action
 */
export function DashboardReducer(state: DashboardDataState = InitialDashboardDataState, action: ActionWithPayload<DashboardData>): DashboardDataState {
    let newState:DashboardDataState = {
      dashboardData: {...state.dashboardData}
    };
    switch (action.type) {
        case DashboardAction.DASHBOARD_SET_DATA:
          newState.dashboardData = action.payload;
          return newState;
        case DashboardAction.DASHBOARD_SET_DATA_RTW:
          newState.dashboardData = action.payload;
          return newState;
        default:
          return newState;
    }
}
