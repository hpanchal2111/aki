
/**
 * @description Interface for DashboardData
 */
export interface DashboardData {
    walking?: number;
    stepsThisSession?: number;
    totalSessions?: number;
    lastSessions?: String;
    stepsLastSessions?: number;
    walkingWithScuffing?: number;
    rightLimpWalking?: number;
    leftLimpWalking?: number;
    rightTrendelenbergWalking: number;
    running?: number;
    showCrossLegsTile?: String;
    lastDateTimeCrossLegsDone?: String;
    crossLegsAverageTimeBetweenViolations?: String;
    barChartData?: any;
    lineChartData?: any;
    pieChartData?: any;
    polarChartData?: any;
}

export interface DashboardDataState {
    dashboardData: DashboardData;
}

/**
 * @description Initial state for DashboardData
 */
export const InitialDashboardDataState: DashboardDataState = {
    dashboardData: {
        walking: 0,
        stepsThisSession: 0,
        totalSessions: 0,
        lastSessions: "Never",
        stepsLastSessions: 0,
        walkingWithScuffing: 0,
        rightLimpWalking: 0,
        leftLimpWalking: 0,
        rightTrendelenbergWalking: 0,
        running: 0,
        showCrossLegsTile: "hide",
        lastDateTimeCrossLegsDone: "0",
        crossLegsAverageTimeBetweenViolations: "0",
        barChartData: {
            title: "Bar Chart Title",
            labels: ["Red", "Blue", "Green"],
            data: [1, 5, 6],
        },
        lineChartData: {
            title: "Line Chart Title",
            labels: ["Red", "Blue", "Green"],
            data: [1, 5, 6],
        },
        pieChartData: {
            title: "Pie Chart Title",
            labels: ["Red", "Blue", "Green"],
            data: [1, 5, 6],
        },
        polarChartData: {
            title: "Polar Chart Title",
            labels: ["Red", "Blue", "Green"],
            data: [1, 5, 6],
        },
    }
};
