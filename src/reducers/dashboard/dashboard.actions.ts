/**
 * Actions related to dashboard page
 * @type {string}
 */

export const DASHBOARD_GET_DATA = 'DASHBOARD_GET_DATA';
export const DASHBOARD_SET_DATA = 'DASHBOARD_SET_DATA';
export const DASHBOARD_SET_DATA_RTW = 'DASHBOARD_SET_DATA_RTW';
