import {DeviceData} from "../../../reducers/devices/data/device-data.state";

export interface DataStreamInterface {
  success:boolean,
  params:Array<DeviceData>
}
