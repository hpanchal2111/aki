/**
 * @author William Tempest Wright Ferrer
 */
import {SweetSpotProvider} from "../../providers/sweet-spot/sweet-spot.provider";
import {PeripheralInterface} from "../peripheral/peripheral.interface";
import {LocationType} from "../../types/location.type";
import {AssignBytesType} from "../../types/assign-bytes.type";

export interface DeviceInterface {
  peripheral: PeripheralInterface;
  write_char_uuid: string;
  write_service_uuid: string;
  notify_char_uuid: string;
  notify_service_uuid: string;
  read_char_uuid: string;
  read_service_uuid: string;
  provider: SweetSpotProvider;
  battery:string;
  dataToCapture:Array<string>;
  location:LocationType;
  frequency:number;
  isConnected:boolean;
  assignedBits:AssignBytesType;
  isStreaming:boolean;
  isPaired:boolean;
  rssi:number;
  isPolling:boolean;
  requestedStreaming:boolean;

  /**
   * Direction: App to Sensor
   * Format: “DD&gt;GET”. This is sent to the sensor when the app first connects to it. The
   * sensor sends the &lt;DEVICE DETAILS&gt; command in response.
   */
  ddGet();


  /**
   * Direction: App to Sensor
   * Format: “SLF>SET>[FREQUENCY]”
   * Example: “SLF>SET>20”. This sets the Sensor Log frequency on the sensor to 20
   * hertz.
   * Response: “SLF>20”
   * @param {number} frequency
   */
  sensorLogFrequency(frequency:number);

  /**
   * Direction: App to Sensor
   * Format: “SA>SET>[DISPLAY DATA BYTES]”
   * Example: “SA>SET>0110110”. This tells the sensor it’s been assigned to a position, also
   * sending along a byte that demonstrates which types of display data the sensor should be
   * sending during the session.
   * Response: “SA>0110110”
   * Details: [DISPLAY DATA BYTES] is a 7 bit binary string. 1 indicates collection of a
   * particular measurement.
   * DISPLAY_DATA_BYTES[0]  Acc_x measurement
   * DISPLAY_DATA_BYTES[1]  Acc_y measurement
   * DISPLAY_DATA_BYTES[2]  Acc_z measurement
   * DISPLAY_DATA_BYTES[3]  Gyr_x measurement
   * DISPLAY_DATA_BYTES[4]  Gyr_y measurement
   * DISPLAY_DATA_BYTES[5]  Gyr_z measurement
   * DISPLAY_DATA_BYTES[6]  Unused
   * @param {string} displayBytes
   */
  sensorAssignment(displayBytes:string);

  /**
   * Direction: App to Sensor
   ^ Format: “STRT”. This lets the sensor know that the app is ready to receive data in the
   * form of <SENSOR LOG> commands
   */
  streamStart();

  /**
   * Direction: App to Sensor
   * Format: “STOP”. This lets the sensor know that the app no longer needs to receive data
   * in the form of <SENSOR LOG> commands
   */
  streamStop();

  /**
   * Direction: App to Sensor
   * Format: “BLINK”. This tells the sensor to blink its LED lights for two seconds.
   */
  sensorCheck();

  /**
   * starts listening for notifications
   */
  onNotify(data:Array<any>);

  /**
   * call back for reading an RSSI from the device
   * @param {number} rssi
   */
  onReadRSSI (rssi:number);

  /**
   * @description Reads the RSSI via the provider
   */
  readRSSI ():void;

  /**
   * starts listening for READ response
   */
  onRead(data:any);

  /**
   * triggers on connect to device
   * @param data
   */
  onConnect(data:PeripheralInterface);

  /**
   * triggers on connect to device
   * @param data
   */
  onDisconnect(data?:any);

  /**
   * gets the peripherals id
   * @returns {string}
   */
  getPeripheralId():string;

  /**
   * @description connects to the device
   */
  connectToDevice():void;

  /**
   * @description disconnects from device
   */
  disconnectDevice():void;
}
