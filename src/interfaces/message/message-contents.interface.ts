export interface RealTimeWebMessage {
  message?:string;
  interpolateParams?:Object;
}
/**
 * @description message content interface
 */
 export interface MessageContentsInterface {
  type:string,
  payload?:RealTimeWebMessage
}


