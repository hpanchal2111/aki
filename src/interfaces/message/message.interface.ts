import { MessageContentsInterface } from "./message-contents.interface";

/**
 * @description message interface
 */
export interface MessageInterface {
  channel:string,
  message?:MessageContentsInterface,
  publisher:string,
  subscribedChannel:string,
  timetoken: string 
}

