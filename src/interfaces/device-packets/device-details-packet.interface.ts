/**
 * @author William Tempest Wright Ferrer
 */
export interface DeviceDetailsPacketInterface {
  device_id:string;
  sensor_data:Array<string>
}
