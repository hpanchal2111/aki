/**
 * @author William Tempest Wright Ferrer
 */
import {Action} from "@ngrx/store";

export interface DeviceDataAction<DeviceTimeData> extends Action{
  type: string;
  payload: DeviceTimeData;
  error?: boolean;
  meta?: Object;
}
