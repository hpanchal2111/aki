/**
 * @author William Tempest Wright Ferrer
 */
import {Action} from "@ngrx/store";

export interface ActionWithPayload<P> extends Action{
  type: string;
  payload: P;
  error?: boolean;
  meta?: Object;
}
