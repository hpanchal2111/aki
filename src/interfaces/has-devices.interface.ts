/**
 * @author William Tempest Wright Ferrer
 */

import { Store } from "@ngrx/store";
import { DeviceDiscoveredState } from "../reducers/devices/discovered/device-discovered.state";
import { ToastProvider } from "../providers/toast/toast.provider";
import { NgZone } from "@angular/core";
import {DeviceInterface} from "./models/device.interface";

export interface HasDevicesInterface{
  deviceDiscoveredStore:Store<DeviceDiscoveredState>;
  toastProvider: ToastProvider;
  devices: Array<DeviceInterface>;
  zone:NgZone;
}
