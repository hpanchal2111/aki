import { Injectable } from '@angular/core';
import { BackgroundProcessConstants } from '../../constants/background-process.constants';
import { Store } from "@ngrx/store";
import {
  getDeviceDiscoveredStateContents,
  getDevicesLostConnection
} from "../../reducers";
import { DeviceInfoState } from "../../reducers/devices/info/device-info.state";
import { DeviceDiscoveredInfo, DeviceDiscoveredState } from "../../reducers/devices/discovered/device-discovered.state";
import { DeviceInterface } from "../../interfaces/models/device.interface";
import { DeviceModel } from "../../models/device.model";
import { DeviceConstants } from "../../constants/device.constants";
import { DeviceDataState } from "../../reducers/devices/data/device-data.state";
import { ToastProvider } from "../toast/toast.provider";
import { BLE } from "@ionic-native/ble";
import { SweetSpotProvider } from "../sweet-spot/sweet-spot.provider";
import { MachineLearningSessionState } from "../../reducers/machine-learning-session/machine-learning-session.state";
import { Subscription } from "rxjs/Subscription";
import { interval } from 'rxjs/observable/interval';
import { Observable } from "rxjs/Observable";


/*
  Generated class for the UploadBackgroundProcessProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DevicePollingProvider {

  /**
   * interval observable
   */
  protected interval:Observable<number>;
  constructor(
    private deviceInfoStore: Store<DeviceInfoState>,
    private deviceDiscoveredStore: Store<DeviceDiscoveredState>,
    private ble: BLE,
    private bleProvider: SweetSpotProvider,
    private deviceDataStore: Store<DeviceDataState>,
    //private deviceInfoStore: Store<DeviceInfoState>,
    private machineLearningSessionStore: Store<MachineLearningSessionState>,
    public toastProvider: ToastProvider,
  ) {
  }

  /**
   * @description  starts the queue
   */
  startQueue() : void {
    this.interval = interval(BackgroundProcessConstants.INTERVAL_TIME);
    this.interval.subscribe(() => {
      this.pollDevices();
      this.disconnectDevices();
    });
  }


  /**
   * @description disconnects dormant devices
   */
  protected disconnectDevices ():void {
    let deviceIds:Array<string>;
    let subscription:Subscription = this.deviceInfoStore.select(getDevicesLostConnection).subscribe(s => deviceIds = s);
    subscription.unsubscribe();
    let deviceDiscovered:DeviceDiscoveredInfo = this.getDeviceDiscovered();
    for (let key in deviceIds) {
      let id:string = deviceIds[key];
      if (deviceDiscovered.hasOwnProperty(id)) {
        let device:DeviceInterface = deviceDiscovered[id];
        device.disconnectDevice();
      }
    }
  }
  /**
   * @description polls the current connected devices for RSSI
   */
  protected pollDevices ():void {
    let deviceDiscovered:DeviceDiscoveredInfo = this.getDeviceDiscovered();
    for (let key in deviceDiscovered) {
      let device:DeviceInterface = deviceDiscovered[key];
      device.readRSSI();
    }
  }
  /**
   * @description gets the currently connected devices
   * @returns {DeviceDiscoveredState}
   */
  protected getDeviceDiscovered ():DeviceDiscoveredInfo {
    let stateSlice:DeviceDiscoveredInfo;
    let subscription:Subscription = this.deviceDiscoveredStore.select(getDeviceDiscoveredStateContents).subscribe(s => stateSlice = s);
    subscription.unsubscribe();
    return stateSlice;
  }


  /**
   * scans for devices using the ble provider
   */
  scan() {
    console.warn('I am here!');
    //this.devices = [];  // clear list
    this.ble.stopScan().then(data => this.onScanStop(), data => this.onScanStop);

  }

  protected onScanStop(){
    this.ble.startScanWithOptions([], {
      reportDuplicates: true
    }).subscribe(
      peripheral => this.onDeviceDiscovered(peripheral),
      error => {
        this.toastProvider.errorMessage('ERROR_SCAN_BLUETOOTH');
        this.scan();
      }
    );
  }

  /**
   * fires when a device is discovered. It makes a new model with the info about the peripheral. The rest of the interactions with the peripheral happen through the model.
   * @param peripheral
   */
  onDeviceDiscovered(peripheral) {
    if (DeviceConstants.DEVICE_NAMES.indexOf(peripheral.name) > -1) {
      let deviceDiscovered:DeviceDiscoveredInfo = this.getDeviceDiscovered();
      if (deviceDiscovered.hasOwnProperty(peripheral.id) === false) {
        console.log('Discovered ' + JSON.stringify(peripheral, null, 2));
        new DeviceModel(this.bleProvider, peripheral, this.deviceDiscoveredStore, this.deviceDataStore, this.machineLearningSessionStore);
      }
    }
  }
}
