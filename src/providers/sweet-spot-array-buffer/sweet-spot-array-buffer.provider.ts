/**
 * @author William Tempest Wright Ferrer
 */
import { Injectable } from '@angular/core';
import {BufferArrayProvider} from "../buffer-array/buffer-array";
import {DeviceConstants} from "../../constants/device.constants";

export interface BufferInterface {
  [index: string]:string
}
/*
  Generated class for the SweetSpotArrayBufferProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.

*/
@Injectable()
/**
 * @description This provider handles the parsing of data going back and forth between the SS device and the mobile app
 */
export class SweetSpotArrayBufferProvider {

  protected buffer:BufferInterface = {};

  constructor(
    private arrayBufferProvider:BufferArrayProvider
  ) {}

  /**
   * @description Takes data it receives, and checks to see if it's part of a buffer of data coming in from the device. If it is it adds to the buffer. It returns the data from the buffer when it encounters &&& at the end of the string.
   * @param {ArrayBuffer} data
   * @param {string} peripheralId
   * @returns {Array<any>}
   */
  public parseReceivedData (data:ArrayBuffer, peripheralId:string):Array<any>
  {
    let string:string = this.arrayBufferProvider.bufferToString(data);
    // If it ends in &&& then we get any buffer saved already, and append this to it, then send it to notify on the device
    if (DeviceConstants.TRIM_END_OF_DATA_REG_EXP.test(string)) {
      string = this.getBuffer(peripheralId) + string;
      this.clearBuffer(peripheralId);
      return this.prepareReceivedData(string);
      // Other wise we add to a running buffer for this device
    } else {
      // If we see the start of a new command then clear the old buffer. This should clear corrupted data from lack of retrieving data mid stream
      if (DeviceConstants.START_OF_COMMAND_REG_EXP.test(string)) {
        this.clearBuffer(peripheralId);
      }
      this.addToBuffer(peripheralId, string)
    }
    return [];
  }

  /**
   * @description clears the buffer of data that has been streamed back on notify from a sweetspot device.
   * @param {string} peripheralId
   */
  protected clearBuffer (peripheralId:string):void
  {
    this.buffer[peripheralId] = '';
  }

  /**
   * @description adds the buffer of data that has been streamed back on notify from a sweetspot device.
   * @param {string} peripheralId
   * @param data
   */
  protected addToBuffer (peripheralId:string, data:string):void
  {
    if (this.buffer.hasOwnProperty(peripheralId)=== false) {
      this.buffer[peripheralId] = '';
    }
    this.buffer[peripheralId] += data;
  }

  /**
   * @description gets the buffer of data that has been streamed back on notify from a sweetspot device.
   * @param {string} peripheralId
   */
  protected getBuffer (peripheralId:string):string
  {
    if (this.buffer.hasOwnProperty(peripheralId)=== false) {
      return '';
    }
    return this.buffer[peripheralId];
  }

  /**
   * @description prepares data received from device
   * @param {ArrayBuffer} string
   * @returns {Array<any>}
   */
  protected prepareReceivedData (string:string):Array<any> {
    //Device Details should have been formatted DD>{ by Breadware, but they formatted it DD{. This line fixes this.
    string = string.replace(DeviceConstants.FIX_DD_REG_EX_FIND, DeviceConstants.FIX_DD_REG_EX_REPLACE);
    string = string.replace(DeviceConstants.TRIM_END_OF_DATA_REG_EXP, '');
    let array = string.split(DeviceConstants.SEPARATOR);
    array.forEach(function (
      elem:string,
      index:number,
      array:(string|Array<string>|Object)[]
    ) {
      if (elem.includes('{')) {
        array[index] = JSON.parse(elem);
      } else if (elem.includes(DeviceConstants.SUB_SEPARATOR)) {
        array[index] = elem.split(DeviceConstants.SUB_SEPARATOR);
      }
    });
    //console.warn('Normalized Notify Data', array);
    return array;
  }

  /**
   * @description packs the data and adds the required append to the end.
   * @returns {ArrayBuffer}
   * @param command
   * @param payload
   */
  public prepareDataForWrite(command:string, payload:string|number|Object):ArrayBuffer {
    if (typeof payload === "object") {
      payload = JSON.stringify(payload);
    }
    const string:string = command + payload + DeviceConstants.APPEND_TO_DATA;
    console.log('string command', string);
    return this.arrayBufferProvider.stringToBuffer(string);
  }
}
