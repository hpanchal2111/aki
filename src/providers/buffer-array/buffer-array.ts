import { Injectable } from '@angular/core';

/*
  Generated class for the BufferArrayProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class BufferArrayProvider {

  public stringToBuffer(str:string):ArrayBuffer {
    let array = new Uint8Array(str.length);
    for (let i = 0, l = str.length; i < l; i++) {
      array[i] = str.charCodeAt(i);
    }
    return array.buffer;
  }

  public bufferToString (buffer:ArrayBuffer):string {
    return String.fromCharCode.apply(null, new Uint8Array(buffer));
  }
}
