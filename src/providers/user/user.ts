import { Injectable } from '@angular/core';
import { ApiProviderAbstract } from '../api/api.provider.abstract';

/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {

  constructor(
    private api: ApiProviderAbstract
  ) {}

  getUser(id: void) {
    return this.api.get('some_endpoint.com' + id);
  }

}
