import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Observable";
import {GenericResponse} from "../../interfaces/http/response/generic-response.interface";
import {ResourceProviderAbstract} from "../resource/resource.provider.abstract";
import {MachineLearningApiProvider} from "../api/machine-learning/machine-learning-api.provider";

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider extends ResourceProviderAbstract{
	protected resourceName:string = 'login';
  constructor(
    public api: MachineLearningApiProvider
  ) {super();}

  /**
   * @description call login api
   */
  login(data):Observable<GenericResponse>{
    return this.api.post(this.resourceName, data);
  }

  /**
   * @description call sigup api
   */
  signup(data):Observable<GenericResponse>{
    return this.api.post(this.resourceName, data);
  }

  /**
   * @description call login with facebook api
   */
  fblogin(data):Observable<GenericResponse>{
    return this.api.post(this.resourceName, data);
  }

  /**
   * @description call login with facebook api
   */
  gpluslogin(data):Observable<GenericResponse>{
    return this.api.post(this.resourceName, data);
  }

}
