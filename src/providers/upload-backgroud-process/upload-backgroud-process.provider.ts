import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BackgroundProcessConstants } from '../../constants/background-process.constants';
import { DataProvider} from "../resource/machine-learning/data.provider";
import { DeviceData, DeviceDataState, DeviceTimeData } from "../../reducers/devices/data/device-data.state";
import { Store} from "@ngrx/store";
import { getDeviceDataStateQueued, getDeviceDataStateHeadCollection } from "../../reducers";
import { DEVICE_QUEUE_DATA, DEVICE_REMOVE_QUEUE_DATA } from "../../reducers/devices/data/device-data.actions";
import { ToastProvider } from "../toast/toast.provider";
import { Subscription } from "rxjs/Subscription";
import { DataStreamInterface } from "../../interfaces/http/request/data-stream.interface";
import { Observable } from "rxjs/Observable";
import { interval } from "rxjs/observable/interval";

/*
  Generated class for the UploadBackgroundProcessProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UploadBackgroundProcessProvider {


  /**
   * tracks if there is an api request outstanding to prevent duplicate sending of data
   * @type {boolean}
   */
  protected waiting:boolean = false;
  /**
   * interval observable
   */
  protected interval:Observable<number>;
  constructor(
    public http: HttpClient,
    private dataProvider:DataProvider,
    private deviceDataStore: Store<DeviceDataState>,
    private toastProvider:ToastProvider
  ) {
  }

  /**
   * @description  starts the queue
   */
  startQueue() : void {
    this.interval = interval(BackgroundProcessConstants.INTERVAL_TIME);
    this.interval.subscribe(() => {
      if (this.waiting === false) {
        //TODO: Really upload this data
        let stateSlice:DeviceTimeData = this.getQueueData();
        if (Object.keys(stateSlice).length) {
          console.log('slice of life', stateSlice);
          let data:Array<DeviceData> = [];
          for (let key in stateSlice) {
            data.push(stateSlice[key]);
          }
          let payload:DataStreamInterface = {
            success:true,
            params:data
          };
          this.waiting = true;
          this.dataProvider.create(payload).subscribe(
            res => {
              //console.log('response for data upload queue', res);
              if (res.success === true) {
                this.waiting = false;
                this.clearQueuedData(stateSlice);

              }
            },
            err => {
              console.log('Upload error: ', err);
              this.waiting = false;
              this.toastProvider.errorMessage('ERROR_CANT_UPLOAD_MOVEMENT_DATA');
            }
          );
        }
      }

    });
  }

  /**
   * @description  clears the queued data that that has been uploaded
   * @param {DeviceTimeData} stateSlice
   */
  protected clearQueuedData (stateSlice:DeviceTimeData) {
    this.deviceDataStore.dispatch({'type': DEVICE_REMOVE_QUEUE_DATA, 'payload':stateSlice});
  }
  /**
   * @description get queued data, if no queued data it queues some and returns it.
   * @returns {DeviceTimeData}
   */
  protected getQueueData():DeviceTimeData {
    let stateSlice:DeviceTimeData = {};
    let subscription:Subscription = this.deviceDataStore.select(getDeviceDataStateQueued).subscribe(s => stateSlice = s);
    subscription.unsubscribe();
    if (Object.keys(stateSlice).length === 0) {
      this.deviceDataStore.select(getDeviceDataStateHeadCollection).subscribe(s => stateSlice = s);
      if (Object.keys(stateSlice).length !== 0) {
        this.deviceDataStore.dispatch({'type': DEVICE_QUEUE_DATA, 'payload':stateSlice});
      }
    }
    return stateSlice;
  }
  /**
   * Posts data to a set constant interval and URL
   *
   * @param postData
   */
  /*post(postData: any): void {

    setInterval(() => {

      this.http.post(BackgroundProcessConstants.postUrl, postData).subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log("Error occured");
        }
      );

    }, BackgroundProcessConstants.INTERVAL_TIME);

  }*/

}
