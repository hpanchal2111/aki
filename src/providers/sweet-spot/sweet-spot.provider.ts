/**
 * @author William Tempest Wright Ferrer
 */

import { Injectable } from '@angular/core';
import { BLE } from '@ionic-native/ble';
import {DeviceInterface} from "../../interfaces/models/device.interface";
import {SweetSpotArrayBufferProvider} from "../sweet-spot-array-buffer/sweet-spot-array-buffer.provider";
import {ToastProvider} from "../toast/toast.provider";
import {DeviceConstants} from "../../constants/device.constants";


/*
  Generated class for the SweetSpotProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
  This class handles communication to and from the SweetSpotDevice. It normalizing the data being passed back and forth and is used to connect the Device to a model.
*/
@Injectable()
/**
 * @description This provider handles communication between a SS device and a model that represents that device
 */
export class SweetSpotProvider {

  constructor(
    private ble: BLE,
    private arrayBufferProvider:SweetSpotArrayBufferProvider,
    private toastProvider:ToastProvider
  ) {}

  /**
   * @description connects to device
   * @param {DeviceInterface} device
   */
  connectToDevice(device:DeviceInterface) {
    console.log('device ', device);
    //TODO: Real error handling
    this.ble.connect(device.getPeripheralId())
      .subscribe(data=>{
        device.onConnect(
          data
        )
      },
        data =>{
          this.toastProvider.errorMessage('ERROR_CANT_CONTACT_DEVICE');
        })
  }

  /**
   *  @description reads from device, Not used
   * @param {DeviceInterface} device
   */
  /*readDevice(device:DeviceInterface) {
    this.ble.read(device.getPeripheralId(), device.read_service_uuid, device.read_char_uuid)
      .then(data=>{
        device.onRead(
          SweetSpotProvider.prepareDataFromResponse(data)
        )
      },
        data =>{})
  }*/


  /**
   * reads rssi for a device and then calls it's onReadRSSI method
   * @param {DeviceInterface} device
   */
  readRSSI(device:DeviceInterface):void {
    this.ble.readRSSI(device.getPeripheralId()).then( rssi=>{
        device.onReadRSSI(rssi);
      },
      data => {
        device.onReadRSSI(DeviceConstants.RSSI_NO_SIGNAL);
        //this.disconnectDevice(device);
        //this.toastProvider.errorMessage('ERROR_CANT_READ_RSSI');
      }
    );
  }

  /**
   * @description notify from device
   * @param {DeviceInterface} device
   */
  notifyDevice(device:DeviceInterface) {
    //TODO: Real error handling
    this.ble.startNotification(device.getPeripheralId(), device.notify_service_uuid, device.notify_char_uuid)
      .subscribe(data=>{
          let array:Array<any> = this.arrayBufferProvider.parseReceivedData(data, device.getPeripheralId());
          if (array.length > 0) {
            //try {
              device.onNotify(
                array
              )
            //} catch (e) {
              //this.toastProvider.errorMessage(e);
            //}
          }
      },
        data =>{
          this.toastProvider.errorMessage('ERROR_CANT_CONTACT_DEVICE');
        })
  }

  /**
   * @description disconnect device
   * @param {DeviceInterface} device
   */
  disconnectDevice(device:DeviceInterface) {
    //TODO: Real error handling
    this.ble.disconnect(device.getPeripheralId())
      .then(data=>{
        //let string:string = ArrayBufferConverter.bufferToString(data);
        device.onDisconnect(
          //SweetSpotProvider.prepareDataFromResponse(string)
        )
      },
        data =>{
          this.toastProvider.errorMessage('ERROR_CANT_CONTACT_DEVICE');
        })
  }

  /**
   * @description writes to device
   * @param {DeviceInterface} device
   * @param {string} command
   * @param {string | number | Object} payload
   * @param {Function} callback
   * @param {number} failures
   */
  writeDevice(device:DeviceInterface, command:string, payload:string|number|Object, callback:Function, failures:number=0) {
    //TODO: Real error handling
    const data = this.arrayBufferProvider.prepareDataForWrite(command, payload);

    this.ble.write(device.getPeripheralId(), device.write_service_uuid, device.write_char_uuid, data)
    .then(data=>{
      console.log('command success', command, payload);
      callback(
        true
        //SweetSpotProvider.prepareDataFromResponse(data)
      )
    },
      data =>{
      console.warn('command failure', command, payload);
        if (failures >= DeviceConstants.MAX_RETRIES) {
          this.disconnectDevice(device);
          this.toastProvider.errorMessage('ERROR_CANT_CONTACT_DEVICE');
        } else {
          failures++;
          this.writeDevice(device, command, payload, callback, failures);
        }
      })
  }

  /**
   * @description Not used
   * WRITE with out response
   * @param {DeviceInterface} device
   * @param command
   * @param payload
   * @param {Function} callback
   */
  /*writeWithoutResponse(device:DeviceInterface, command:string, payload:string|number|Object, callback:Function) {
    //TODO: Do we need a response, it is WRITE with out response?
    //TODO: Real error handling
    let data = SweetSpotProvider.prepareDataForWrite(command, payload);
    this.ble.writeWithoutResponse(device.getPeripheralId(), device.write_service_uuid, device.write_char_uuid, data)
      .then(data=>{
        callback(
          SweetSpotProvider.prepareDataFromResponse(data)
        )
      },
          data => {})
  }*/






}
