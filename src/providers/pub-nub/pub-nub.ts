import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PubNubAngular } from 'pubnub-angular2';
import { PubNubConstants } from '../../constants/pubnub.constants';


export interface PubNubSubscribeArgs {
    channels?: Array<string>,
    triggerEvents?: boolean,
    withPresence?: boolean
}

// TODO: If we have need of this prevention of duplication functionality I wrote here in the future, it should instead be handled by an NGRX store. I am making this functionality redundant by unsubscribe after we get an update.
/**
 * PubNubAngular Provider
*/
@Injectable()
export class PubNubProvider {

  constructor(
    public http: HttpClient,
    public pubnubAngular: PubNubAngular
  ) {

  }

  /**
   * Initialize pubnub with publish and subscribe key
   */
  init(uuid:number): PubNubAngular {
    this.pubnubAngular.init({
      publishKey: PubNubConstants.PUBLISH_KEY,
      subscribeKey: PubNubConstants.SUBSCRIBE_KEY,
      uuid: uuid
    });
    return this.pubnubAngular;
  }

    /**
     * subscribes to a PubNub channel
     * @param {PubNubSubscribeArgs} args
     */
  subscribe(args:PubNubSubscribeArgs):void {
      this.pubnubAngular.subscribe(args);
  }

    /**
     *
     * @param {string} channel
     */
  unsubscribe(channel:string):void {
    this.pubnubAngular.unsubscribe(channel);
  }

    /**
     * gets messages on a channel wiring them to a call back
     * @param {string | string[]} channel
     * @param {(message: any) => void} callback
     * @returns {any[]}
     */
  getMessage(channel: string | string[], callback: (message: any) => void): any[] {
    return this.pubnubAngular.getMessage(channel, callback);
  }

}
