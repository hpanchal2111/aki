import {ApiProviderAbstract} from "../api.provider.abstract";
import {Injectable} from "@angular/core";
import {ENV} from "../../../../.env";

@Injectable()
export class ProfileProvider extends ApiProviderAbstract {
  baseUrl:string = ENV.PROFILE_API_URL;
}
