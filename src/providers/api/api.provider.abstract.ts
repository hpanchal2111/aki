/**
 * @author Marat.Bokov
 * @description API provider for the AKI App
 */

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { GenericResponse } from "../../interfaces/http/response/generic-response.interface";

/*
  Generated class for the ApiProviderAbstract provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export abstract class ApiProviderAbstract {
  abstract baseUrl: string;

  constructor(
    public http: HttpClient
  ) {}

  /**
   * @description Get method for the API
   * @param endpoint string for the endpoint for the get method
   * @param options
   */
  get(endpoint: string, options?:object):Observable<GenericResponse> {
    return this.http.get<GenericResponse>(this.baseUrl + endpoint, options);
  }

  /**
   * @description Post Method for the AKI API
   * @param endpoint
   * @param data
   * @param options
   */
  post(endpoint: string, data: object, options?:object):Observable<GenericResponse> {
    return this.http.post<GenericResponse>(this.baseUrl + endpoint, data, options);
  }

  /**
   * @description Put Method for the AKI API
   * @param endpoint
   * @param data
   * @param options
   */
  put(endpoint: string, data: object, options?:object):Observable<GenericResponse> {
    return this.http.put<GenericResponse>(this.baseUrl + endpoint, data, options);
  }

  /**
   * @description Delete Method for the AKI API
   * @param endpoint
   * @param options
   */
  delete(endpoint: string, options?:object):Observable<GenericResponse> {
    return this.http.delete<GenericResponse>(this.baseUrl + endpoint, options);
  }

}
