import {ApiProviderAbstract} from "../api.provider.abstract";
import {Injectable} from "@angular/core";
import {ENV} from "../../../../.env";

@Injectable()
export class MachineLearningApiProvider extends ApiProviderAbstract {
  baseUrl:string = ENV.ML_API_URL;
}
