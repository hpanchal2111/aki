import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import { UserDataState, UserData } from '../../../reducers/user/user.state';
import { getUserDataStateContents } from '../../../reducers';
import { SessionState } from '../../../reducers/session/session.state';
import { ToastProvider } from '../../../providers/toast/toast.provider';
import * as SessionActions from '../../../reducers/session/session.actions';
import { ENV } from "../../../../.env";

@Injectable()
export class SessionApiProvider {
  // stores user data to be used on the view
  userData: UserData;	
  constructor(
  	public userStore: Store<UserDataState>,
  	public sessionStore: Store<SessionState>,
  	public toast: ToastProvider
  ) {
    // get user data
    userStore.select(getUserDataStateContents).subscribe(
      (data: UserData) => {
        this.userData = data;
      },
      error => {
        this.toast.errorMessage('USER_GET_USER_ERROR');
      }
    );  	
  } 

  /**
   * @description generate JWT bearer token
   */
  public getJWTToken()
  {
  	if(this.userData)
  	{
			var JWTHelper = require('jwthelper');
			// Create a new helper and set the options for that helper 
			var helper = JWTHelper.createJWTHelper({
			    'secret': ENV.JWT_SECRET
			});
			// Signing a JWT 
			var jwt = helper.sign(this.userData);
			var sessionData = {bearerToken: jwt, ReceivedAtTimeStamp: Date.now()};
			this.sessionStore.dispatch({type: SessionActions.SET_SESSION, payload: sessionData}) 	
		}
		else
		{
      this.toast.errorMessage('USER_GET_USER_ERROR');
		}	
  }
}
