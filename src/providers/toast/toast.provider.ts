import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
/*
  Generated class for the ToastProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ToastProvider {

  constructor(
    public toast: ToastController,
    public translate: TranslateService
  ) {

  }

  /*onDeviceDisconnected(peripheral) {
    let toast = this.toast.create({
      position: 'bottom',
      message: 'The peripheral unexpectedly disconnected',
      duration: 3000
    });
    toast.present();
  }*/

  /**
   * Creates a custom toast
   *
   * @param  {string} message translation key
   * @param translate
   * @param interpolateParams
   * @param {"top" | "middle" | "bottom"} position
   * @param  {number=3000} duration=3000
   * @param  {string} cssClass?
   * @returns void
   */
  customMessage(message: string, translate:boolean = false, interpolateParams: Object = {}, cssClass?: string, position: 'top' | 'middle' | 'bottom' = 'bottom', duration = 3000): void {

    // get translation
    if (translate===true) {
      this.translate.get(message, interpolateParams).subscribe((translatedMsg: string) => {

        this.displayToast(translatedMsg, cssClass, position, duration);

      });
    } else {
      this.displayToast(message, cssClass, position, duration);
    }
  }

  /**
   * Creates a success toast message with custom css
   *
   * @param  {string} message translation key
   * @param translate
   * @param interpolateParams
   * @returns void
   */
  successMessage(message: string, translate:boolean = true, interpolateParams: Object = {}): void {

    if (translate===true) {
      this.translate.get(message, interpolateParams).subscribe((translatedMsg: string) => {

        this.displayToast(translatedMsg,   'toast-success');

      });
    } else {
      this.displayToast(message,   'toast-success');
    }
  }

  /**
   * Creates a error toast message with custom css
   *
   * @param  {string} message translation key
   * @param translate
   * @param interpolateParams
   * @returns void
   */
  errorMessage(message: string, translate:boolean = true, interpolateParams: Object = {}): void {

    if (translate===true) {
      this.translate.get(message, interpolateParams).subscribe((translatedMsg: string) => {

        this.displayToast(translatedMsg, 'toast-error');

      });
    } else {
      this.displayToast(message, 'toast-error');
    }
  }

  /**
   *
   * @param {string} message
   * @param {string} cssClass
   * @param {"top" | "middle" | "bottom"} position
   * @param {number} duration
   */
  protected displayToast (message: string, cssClass?: string, position: 'top' | 'middle' | 'bottom' = 'bottom', duration = 3000, ):void {
    let toast = this.toast.create({
      position: position,
      message: message,
      duration: duration,
      cssClass: 'toast-error'
    });
    toast.present();
  }

}
