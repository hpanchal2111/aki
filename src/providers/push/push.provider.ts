import { Injectable } from "@angular/core";
import { Priority, Push, PushObject } from "@ionic-native/push";
import { PushConstants } from "../../constants/push.constants";
import { TranslateService } from "@ngx-translate/core";

@Injectable()
export class PushProvider {
  isEnabled:boolean;
  pushObject:PushObject;
  constructor(
    private push: Push,
    private translate: TranslateService
  ) {
    this.detectEnabled();
    this.makeChannel();
    this.pushObject = this.push.init(PushConstants.OPTIONS);
  }


  /**
   * Detects whether or not push notifications are enabled
   * @returns {boolean}
   */
  detectEnabled ():boolean{
    this.push.hasPermission()
      .then((res: any) => {
        this.isEnabled = res.isEnabled;
        console.log('push notifications are enabled: ', this.isEnabled)
      });
    return this.isEnabled;
  }

  /**
   * Makes a channel. By default is uses the contents of the PushConstants namespace
   * @param {string} name
   * @param {string} description
   * @param {Priority} importance
   * @param {boolean} translate
   * @returns {Promise<any>}
   */
  makeChannel(
    name:string = PushConstants.DEFAULT_CHANNEL_NAME,
    description:string = PushConstants.DEFAULT_CHANNEL_DESCRIPTION,
    importance:Priority = PushConstants.DEFAULT_CHANNEL_IMPORTANCE,
    translate:boolean = true
  ):  Promise<any> {
    if (translate === true) {
      this.translate.get(name).subscribe((translatedMsg: string) => {
        name = translatedMsg;
      });
      this.translate.get(description).subscribe((translatedMsg: string) => {
        description = translatedMsg;
      });
    }
    return this.push.createChannel({
      id: name,
      description: description,
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: importance
    }).then(() =>
      console.log('push notification channel created', name)
    );

  }
}
