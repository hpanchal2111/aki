import { Injectable } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { ILocalNotification, LocalNotifications } from "@ionic-native/local-notifications";
import { LocalNotificationConstants } from "../../constants/local-notification.constants";

@Injectable()
export class LocalNotificationProvider {
  constructor(
    private translate: TranslateService,
    private localNotifications: LocalNotifications,
  ) {

  }

  /**
   * Schedules a local notification
   * @param {string} text
   * @param {Object} interpolateParams
   * @param {string} title
   * @param {number} id
   * @param {boolean} translate
   * @param {ILocalNotification} additionalOptions
   */
  schedule(
    text:string,
    interpolateParams:Object = {},
    title:string = LocalNotificationConstants.DEFAULT_TITLE,
    id:number = null,
    translate:boolean = true,
    additionalOptions:ILocalNotification = LocalNotificationConstants.DEFAULT_OPTIONS
  ): void {
    if (translate === true) {
      this.translate.get(title, interpolateParams).subscribe((translatedMsg: string) => {
        title = translatedMsg;
      });
      this.translate.get(text, interpolateParams).subscribe((translatedMsg: string) => {
        text = translatedMsg;
      });
    }
    const passedParams:ILocalNotification = {
      id:id,
      title:title,
      text:text,
      data:interpolateParams
    };
    const options:ILocalNotification = Object.assign(passedParams, additionalOptions);
    this.localNotifications.schedule(options)
  }
}
