import { ResourceProviderAbstract } from "../resource.provider.abstract";
import { Injectable } from "@angular/core";
import { ProfileProvider } from "../../api/profile/profile.provider";
import { ModalController} from 'ionic-angular';
import { TrackPostureModalPage } from '../../../pages/track-posture-modal/track-posture-modal';
import { CrossLegsModalPage } from '../../../pages/cross-legs-modal/cross-legs-modal';
import { ModalHelper } from '../../../helper/modal.helper';
import { ShareReportModalPage } from "../../../pages/share-report-modal/share-report-modal";

@Injectable()
export class UserPreferencesProvider extends ResourceProviderAbstract {
  protected modalHelper:ModalHelper;
  protected resourceName:string = 'user-preferences';
  constructor(
    protected api:ProfileProvider,
    public modalCtrl: ModalController
  ) {
    super();
    this.modalHelper =  new ModalHelper(modalCtrl);
  }

  /**
   * @description Open track posture modal
   */
  trackPostureModal()
  {
    this.modalHelper.openModal(TrackPostureModalPage);
  }

  /**
   * @description Open cross legs modal
   */
  crossLegsModal()
  {
    this.modalHelper.openModal(CrossLegsModalPage);
  }

    /**
   * @description Share report modal
   */
  shareReportModal()
  {
    this.modalHelper.openModal(ShareReportModalPage);
  }
}
