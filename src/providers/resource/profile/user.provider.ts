import { ResourceProviderAbstract } from "../resource.provider.abstract";
import { Injectable } from "@angular/core";
import { ProfileProvider } from "../../api/profile/profile.provider";

@Injectable()
export class UserProvider extends ResourceProviderAbstract {
  protected resourceName:string = 'user';
  constructor(
    protected api:ProfileProvider
  ) {
    super();
  }
}
