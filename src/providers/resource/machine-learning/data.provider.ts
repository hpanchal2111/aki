import {ResourceProviderAbstract} from "../resource.provider.abstract";
import {MachineLearningApiProvider} from "../../api/machine-learning/machine-learning-api.provider";
import {Injectable} from "@angular/core";

@Injectable()
export class DataProvider extends ResourceProviderAbstract {
  protected resourceName:string = 'stream-data';
  constructor(
    protected api:MachineLearningApiProvider
  ) {
    super();
  }
}
