/**
 * @author Marat.Bokov
 * @description Provider for users/users list
 */

import { ApiProviderAbstract } from '../api/api.provider.abstract';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { GenericResponse } from "../../interfaces/http/response/generic-response.interface";

/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export abstract class ResourceProviderAbstract {

  /**
   * @description the resource page of the url string
   */
  protected abstract resourceName:string;
  protected abstract api:ApiProviderAbstract;
  constructor(
  ) {}

  /**
   * @description list Users method for listing users
   * @param options
   */
  list(options?:object):Observable<GenericResponse> {
    return this.api.get(this.resourceName, options);
  }

  /**
   * @description get the individual user object
   * @param id number or string of the user ID to pull
   * @param options
   */
  get(id: number | string, options?:object):Observable<GenericResponse> {
    console.log(this.resourceName + '/' + id, options);
    return this.api.get(this.resourceName + '/' + id, options);
  }

  /**
   * @description Create User method to create a user
   * @param data object data for the user to create
   * @param options
   */
  create(data: object, options?:object):Observable<GenericResponse> {
    return this.api.post(this.resourceName, data);
  }

  /**
   * @description update User method to update a user
   * @param id number or string of the user id to update
   * @param data Object Will Will do this?
   * @param options
   */
  update(id: number | string, data: object, options?:object):Observable<GenericResponse> {
    return this.api.put(this.resourceName + '/' + id, data, options);
  }

  /**
   * @description Will will do this
   * @param id number or string
   * @param options
   */
  delete(id: number | string, options?:object ):Observable<GenericResponse> {
    return this.api.delete(this.resourceName + '/' + id, options);
  }

}
