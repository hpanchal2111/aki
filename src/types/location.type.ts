/**
 * @author William Tempest Wright Ferrer
 */
export type LocationType = 'belt' | 'leftShoe' | 'rightShoe' | 'leftWrist' | 'rightWrist' | 'leftShoulder' | 'rightShoulder' | 'chest' | 'neck' | 'head';
