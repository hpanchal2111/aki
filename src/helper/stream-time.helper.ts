/**
 * @author William Tempest Wright Ferrer
 * @copyright 2017 SweetSpot Motion
 */

import {DeviceConstants} from "../constants/device.constants";

/**
 * @description a helper class that can account for the drift on the time keeping of the SSM device by shoring it's time up against the clock on the device running this code.
 */
export class StreamTimeHelper {
  /**
   * @description the time that the first notification came in
   * @type {number}
   */
  firstNotifyTime?:number;
  /**
   * @description the elapsed time that was listed on the first notification that came in
   * @type {number}
   */
  firstStreamElapsedTime?:number;
  /**
   * @description the last time we adjusted the bufferToElapsedTime property
   * @type {number}
   */
  lastSyncTime?:number;
  /**
   * @description the buffer we will add to the elapsed time returned from the device to account for drift on the device
   * @type {number}
   */
  bufferToElapsedTime?:number=0;

  // TODO: consider removing logging features for the release version
  /**
   * @description time pased since the first stream notification was received
   * @type {number}
   */
  timePassedSinceFirstStream:number;

  /**
   * @description what we expect the elapsed time should be returning from the device
   * @type {number}
   */
  elapsedStreamTimeShouldBe:number;

  /**
   * @description elapsed time on the device plus the buffer
   * @type {number}
   */
  elapsedWithBufferAdded:number;

  /**
   * @description the time stamp we actually predict corresponds to the collection time.
   * @type {number}
   */
  derivedTimestamp:number;

  /**
   * @description the derived time since last time
   * @type {number}
   */
  lastDerivedTimestamp:number;

  /**
   * @description the different in derived time since last recorded
   * @type {number}
   */
  derivedTimediff:number;


  /**
   * @description whether or not to log time
   * @type {boolean}
   */
  logTime:boolean;
  /**
   * @description the last time stamp recorded
   * @type {number}
   */
  lastTimestamp?:number;
  /**
   * @description the last time elapsed time recorded
   * @type {number}
   */
  lastElapsed?:number;
  /**
   * @description logging purposes only: Difference since last timestamp recorded
   * @type {number}
   */
  timestampDiff?:number;
  /**
   * @description logging purposes only: Difference since last elapsed time recorded
   * @type {number}
   */
  elapsedDiff?:number;

  /**
   * @description how imprecise is the close on the device
   * @type {number}
   */
  deviceImpricisionPercent?:number;


  constructor (logTime:boolean=true){
    this.logTime = logTime;
  }

  /**
   * @description processes the elapsed time from the packet from the device, adds the starting timestamp from the phone, accounts for drift on the device
   * @param {number} timestamp
   * @returns {number}
   */
  public processTimestamp (timestamp:number):number {
    // Account for problem where despite casting timestamp would be treated as a string
    timestamp =  parseInt(timestamp.toString());
    let currentTime:number = new Date().getTime();
    // Log the first pieces of information on first notify this stream period.
    if (typeof this.firstNotifyTime === 'undefined') {
      this.firstNotifyTime = currentTime;
      this.firstStreamElapsedTime = timestamp;
      this.lastSyncTime = currentTime;
      // If enough time has passed since accounting for the drift on the device adjust the time buffer we apply to the
    } else if (currentTime >= this.lastSyncTime + DeviceConstants.SYNC_STREAM_TIME) {
      // Figure out how much time should have passed since the stream started:
      this.timePassedSinceFirstStream = currentTime - this.firstNotifyTime;
      // Figure out what the elapsed time on the device should have been:
      this.elapsedStreamTimeShouldBe = this.timePassedSinceFirstStream + this.firstStreamElapsedTime;
      // Figure out how much buffer we need to add to get the proper time
      this.bufferToElapsedTime = this.elapsedStreamTimeShouldBe - timestamp;
      this.lastSyncTime = currentTime;
    }

    this.elapsedWithBufferAdded = timestamp + this.bufferToElapsedTime;
    this.derivedTimestamp = this.firstNotifyTime + (this.elapsedWithBufferAdded - this.firstStreamElapsedTime);

    // TODO: consider removing time logging features for release version.
    if (this.logTime === true) {
      this.storeTime(timestamp, currentTime);
    }
    this.lastDerivedTimestamp = this.derivedTimestamp;
    return this.derivedTimestamp;
  }

  // TODO: consider removing time logging features for release version.
  /**
   * @description  Stores time information for logging purposes
   * @param {number} timestamp
   * @param {number} currentTime
   */
  protected storeTime (timestamp:number, currentTime:number):void {
    if (typeof this.lastTimestamp !== undefined) {
      this.timestampDiff = currentTime - this.lastTimestamp;
      this.elapsedDiff = timestamp - this.lastElapsed;
      this.derivedTimediff = this.derivedTimestamp - this.lastDerivedTimestamp;
    }
    this.lastTimestamp = currentTime;
    this.lastElapsed = timestamp;
    this.deviceImpricisionPercent = this.elapsedWithBufferAdded / timestamp;
  }
}
