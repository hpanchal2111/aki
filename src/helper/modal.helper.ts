/**
 * @author William Tempest Wright Ferrer
 * @copyright 2017 SweetSpot Motion
 */
import {Modal} from "ionic-angular/components/modal/modal";
import {ModalOptions} from "ionic-angular/components/modal/modal-options";
import {ModalController} from "ionic-angular";
/**
 * @description a helper class that can account for the drift on the time keeping of the SSM device by shoring it's time up against the clock on the device running this code.
 */
export class ModalHelper {
  protected modal:Modal;
  protected isModalIsOpen:boolean=false;

  constructor(protected modalCtrl:ModalController) {

  }
  /**
   * opens a modal
   * @param component
   * @param data
   * @param {ModalOptions} opts
   */
  openModal (component: any, data?: any, opts?: ModalOptions) {
    if (this.isModalIsOpen === false) {
      this.modal = this.modalCtrl.create(component, data, opts);
      this.isModalIsOpen = true; // for local notifications only
      this.modal.present().then(()=>{
        this.isModalIsOpen = true;
      });
      this.modal.onDidDismiss(()=>{
        this.isModalIsOpen = false;
      })
    }
  }
}
