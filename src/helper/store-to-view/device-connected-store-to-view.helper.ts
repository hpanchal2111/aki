/**
 * @author William Tempest Wright Ferrer
 */
import {DeviceDiscoveredState} from "../../reducers/devices/discovered/device-discovered.state";
import {HasDevicesInterface} from "../../interfaces/has-devices.interface";
import {NgZone} from "@angular/core";
import {DeviceInterface} from "../../interfaces/models/device.interface";
import {getDeviceDiscoveredStateContents} from "../../reducers";

/**
 * @description a helper that binds the data stored in the DeviceDiscoveredState to a components device array
 */
export class DeviceDiscoveredStoreToViewHelper {

  target:HasDevicesInterface;
  constructor (
    target:HasDevicesInterface
  ) {
    this.target = target;
    this.target.zone = new NgZone({ enableLongStackTrace: false });
  }
  /**
   * @description binds the store to the local devices array for display
   */
  bindDeviceDiscoveredStore ():void {
    this.target.deviceDiscoveredStore.select(getDeviceDiscoveredStateContents)
      .subscribe(
        (data:DeviceDiscoveredState) => this.target.zone.run(()=>
          this.setDevice(data)
        ),
        error => {
          this.target.toastProvider.errorMessage('ERROR_CANT_BIND_TO_STORE')
        }
      );
  }

  /**
   * @description when store is updated this binds devices directly
   * @param {DeviceDiscoveredState} data
   */
  setDevice(data:DeviceDiscoveredState):void {
    let newArray: Array<DeviceInterface> = [];
    for (const key in data) {
      newArray.push(data[key])
    }
    this.target.devices = newArray;
  }
}
