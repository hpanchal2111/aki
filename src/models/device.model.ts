/**
 * @author William Tempest Wright Ferrer
 */

import { DeviceInterface } from "../interfaces/models/device.interface";
import { SweetSpotProvider } from "../providers/sweet-spot/sweet-spot.provider";
import { Store } from '@ngrx/store';
import { DeviceDiscoveredState } from "../reducers/devices/discovered/device-discovered.state";
import * as DeviceDiscoveredAction from "../reducers/devices/discovered/device-discovered.actions";
import * as DeviceDataActions from '../reducers/devices/data/device-data.actions'
//import * as DeviceInfoActions from '../reducers/devices/info/device.info.actions'
import { DeviceConstants } from "../constants/device.constants";
import { PeripheralInterface } from "../interfaces/peripheral/peripheral.interface";
//import { DeviceDetailsPacketInterface } from "../interfaces/devicePackets/device-details-packet.interface";
import { DeviceData, DeviceDataState, DeviceTimeData } from "../reducers/devices/data/device-data.state";
import { LocationType } from "../types/location.type";
import { AssignBytesType } from "../types/assign-bytes.type";
//import { DeviceInfoState } from "../reducers/devices/info/device.info.state";
import { MachineLearningSessionState } from "../reducers/machine-learning-session/machine-learning-session.state";
import {getMachineLearningSessionStateContents} from "../reducers";
import {StreamTimeHelper} from "../helper/stream-time.helper";
import {DeviceDetailsPacketInterface} from "../interfaces/device-packets/device-details-packet.interface";



/**
 * @description A model that represents a SweetSpot device. Use this for all interactions to and from a device.
 * Internally this connects to a provider which speaks to the device.
 */
export class DeviceModel implements DeviceInterface {
  peripheral: PeripheralInterface;
  write_char_uuid: string = DeviceConstants.WRITE_CHAR_UUID;
  write_service_uuid: string = DeviceConstants.WRITE_SERVICE_UUID;
  notify_char_uuid: string  = DeviceConstants.NOTIFY_CHAR_UUID;
  notify_service_uuid: string = DeviceConstants.NOTIFY_SERVICE_UUID;
  read_char_uuid: string = DeviceConstants.READ_CHAR_UUID;
  read_service_uuid: string = DeviceConstants.READ_SERVICE_UUID;
  provider: SweetSpotProvider;
  deviceDiscoveredStore: Store<DeviceDiscoveredState>;
  deviceDataStore: Store<DeviceDataState>;
  //deviceInfoStore: Store<DeviceInfoState>;
  machineLearningSessionStore: Store<MachineLearningSessionState>;
  battery:string;
  dataToCapture:Array<string> = DeviceConstants.DEFAULT_DATA_TO_CAPTURE;
  location:LocationType = DeviceConstants.DEFAULT_LOCATION;
  frequency:number;
  isConnected:boolean = false;
  assignedBits:AssignBytesType = '';
  isStreaming:boolean = false;
  machineLearningSessionId:string;
  streamTimeHelper:StreamTimeHelper;
  isPaired:boolean = false;
  rssi:number;
  isPolling:boolean = false;
  requestedStreaming:boolean=false;


  //testing
  //lastTimestamp:number;
  //lastElapsed:number;

  constructor (
    provider: SweetSpotProvider,
    peripheral: PeripheralInterface,
    deviceDiscoveredStore: Store<DeviceDiscoveredState>,
    deviceDataStore: Store<DeviceDataState>,
    //deviceInfoStore: Store<DeviceInfoState>,
    machineLearningSessionStore: Store<MachineLearningSessionState>
  ){
    this.provider = provider;
    this.peripheral = peripheral;
    this.deviceDiscoveredStore = deviceDiscoveredStore;
    this.deviceDataStore = deviceDataStore;
    //this.deviceInfoStore = deviceInfoStore;
    this.machineLearningSessionStore = machineLearningSessionStore;
    // TODO make this get the session id. Not sure if this method will work
    this.machineLearningSessionStore.select(getMachineLearningSessionStateContents).subscribe(machineLearningSessionId =>{
      this.machineLearningSessionId = machineLearningSessionId;
    });
    this.deviceDiscoveredStore.dispatch({type: DeviceDiscoveredAction.DEVICE_DISCOVERED, payload:this});
    this.streamTimeHelper = new StreamTimeHelper();
    this.connectToDevice();
  }

  /**
   * @description connects to the device
   */
  connectToDevice():void {
    this.provider.connectToDevice(this);
  }

  /**
   * @description disconnects from device
   */
  disconnectDevice():void {
    this.provider.disconnectDevice(this);
  }

  /**
   * @description starts listening for notifications
   */
  assignNotifications() {
    this.provider.notifyDevice(this);
  }

  /**
   * @description Direction: App to Sensor
   * Format: “DD>GET”. This is sent to the sensor when the app first connects to it. The
   * sensor sends the <DEVICE DETAILS> command in response.
   */
  ddGet(){
    this.provider.writeDevice(this, DeviceConstants.COMMANDS_TO_DEVICE.ddGet, '', data => {
      //this.dataToCapture = data.sensor_data;
      //this.deviceInfoStore.dispatch({type: DeviceInfoActions.DEVICE_UPDATED, payload:this});
    })
  }

  /**
   * @description Direction: App to Sensor
   * Format: “SLF>SET>[FREQUENCY]”
   * Example: “SLF>SET>20”. This sets the Sensor Log frequency on the sensor to 20
   * hertz.
   * Response: “SLF>20”
   * @param {number } frequency
   */
  sensorLogFrequency(frequency:number=DeviceConstants.SENSORY_LOG_FREQUENCY_DEFAULT){
    console.log('setting sensor frequency', frequency);
    this.provider.writeDevice(this, DeviceConstants.COMMANDS_TO_DEVICE.sensorLogFrequency, frequency, data => {
      console.log('setting sensor frequency callback called', frequency);
      //this.frequency = Number(data[1]);
      //this.deviceInfoStore.dispatch({type: DeviceInfoActions.DEVICE_UPDATED, payload:this});
    })
  }

  /**
   * @description Direction: App to Sensor
   * Format: “SA>SET>[DISPLAY DATA BYTES]”
   * Example: “SA>SET>0110110”. This tells the sensor it’s been assigned to a position, also
   * sending along a byte that demonstrates which types of display data the sensor should be
   * sending during the session.
   * Response: “SA>0110110”
   * Details: [DISPLAY DATA BYTES] is a 7 bit binary string. 1 indicates collection of a
   * particular measurement.
   * DISPLAY_DATA_BYTES[0]  Acc_x measurement
   * DISPLAY_DATA_BYTES[1]  Acc_y measurement
   * DISPLAY_DATA_BYTES[2]  Acc_z measurement
   * DISPLAY_DATA_BYTES[3]  Gyr_x measurement
   * DISPLAY_DATA_BYTES[4]  Gyr_y measurement
   * DISPLAY_DATA_BYTES[5]  Gyr_z measurement
   * DISPLAY_DATA_BYTES[6]  Unused
   * @param {string} displayBytes
   */
  sensorAssignment(displayBytes:string = DeviceConstants.ASSIGN_BYTES_DEFAULT){
    console.log('setting sensor assignment', displayBytes);
    this.provider.writeDevice(this, DeviceConstants.COMMANDS_TO_DEVICE.sensorAssignment, displayBytes, data => {
      console.log('sensor assignment set', displayBytes);
      //this.assignedBits = data[1];
      //this.deviceInfoStore.dispatch({type: DeviceInfoActions.DEVICE_UPDATED, payload:this});
    })
  }

  /**
   * @description Direction: App to Sensor
   ^ Format: “STRT”. This lets the sensor know that the app is ready to receive data in the
   * form of <SENSOR LOG> commands
   */
  streamStart(){
    this.streamTimeHelper = new StreamTimeHelper();
    this.requestedStreaming = true;
    this.provider.writeDevice(this, DeviceConstants.COMMANDS_TO_DEVICE.streamStart, '', data => {
      this.isStreaming = true;
      this.deviceDiscoveredStore.dispatch({type: DeviceDiscoveredAction.DEVICE_UPDATED, payload:this});
    })
  }

  /**
   * @description Direction: App to Sensor
   * Format: “STOP”. This lets the sensor know that the app no longer needs to receive data
   * in the form of <SENSOR LOG> commands
   */
  streamStop(){
    this.requestedStreaming = false;
    this.provider.writeDevice(this, DeviceConstants.COMMANDS_TO_DEVICE.streamStop, '', data => {
      this.isStreaming = false;
      this.streamTimeHelper = new StreamTimeHelper();
      this.deviceDiscoveredStore.dispatch({type: DeviceDiscoveredAction.DEVICE_UPDATED, payload:this});
    })
  }

  /**
   * @description Direction: App to Sensor
   * Format: “BLINK”. This tells the sensor to blink its LED lights for two seconds.
   */
  sensorCheck(){
    this.provider.writeDevice(this, DeviceConstants.COMMANDS_TO_DEVICE.sensorCheck, '', data => {
      // do nothing
    })
  }

  /**
   * @description Listens from notifications from the device that come in from the provider.
   * It takes the notification data that was normalized in the provider and responds accordingly.
   * @param {Array<any>} data
   */
  onNotify(data:Array<any>){
    const command:string = data[0];
    //console.log('notify command', command);
    switch (command) {
      // data to capture are stored on the model and dispatched to the store. The result of a ddGet method call.
      // Deprecating this, it's buggy and we don't get anything valueable out of it. I got a bad json structure from the device just now leading to a bug.
      case DeviceConstants.COMMANDS_FROM_DEVICE.deviceDetails: {
        let deviceData:DeviceDetailsPacketInterface = data[1];
        //TODO: Figure out if the data comes to here
        if (deviceData.hasOwnProperty('sensor_data')) {
          this.dataToCapture = deviceData.sensor_data;
        }
        this.sensorLogFrequency();
        this.sensorAssignment();
        this.deviceDiscoveredStore.dispatch({type: DeviceDiscoveredAction.DEVICE_UPDATED, payload:this});
        break;
      }

      // Stores the primary data that is streamed from the device about the movement data. It passes this data on to the store. The on going result of a streamStart method call
      case DeviceConstants.COMMANDS_FROM_DEVICE.sensorLog: {
        if (this.dataToCapture === undefined || this.location === undefined) {
          throw new Error('ERROR_CAPTURE_DATA_BEFORE_DEVICE_ASSIGNED');
        }

        // end testing
        const dataPayload:DeviceTimeData = this.prepareDataForStorage(data);
        this.deviceDataStore.dispatch({
          type: DeviceDataActions.DEVICE_STORE_DATA,
          payload:dataPayload
        });

        // No really, stop, we told you to stop, so now actually stop.
        if (this.requestedStreaming === false && this.isStreaming === false) {
          this.isStreaming = true;
          this.streamStop();
        }
        break;
      }

      // sensor log frequency stored on the model and dispatched to the store. the result of sensorLogFrequency method call
      case DeviceConstants.COMMANDS_FROM_DEVICE.sensorLogFrequency: {
        this.frequency = Number(data[2]);
        console.log('sensor frequency notify', this.frequency);
        this.deviceDiscoveredStore.dispatch({type: DeviceDiscoveredAction.DEVICE_UPDATED, payload:this});
        break;
      }

      // bytes assigned on the device are stored on the model and dispatched to the store. The result of a sensorAssignment method call
      case DeviceConstants.COMMANDS_FROM_DEVICE.sensorAssignment: {
        this.assignedBits = data[2];
        console.log('sensor assignment notified', this.assignedBits);
        this.deviceDiscoveredStore.dispatch({type: DeviceDiscoveredAction.DEVICE_UPDATED, payload:this});

        //this.ddGet();
        break;
      }

    }

  }

  /**
   * @description Reads the RSSI via the provider
   */
  readRSSI ():void {
    if (this.isPolling === false) {
      this.provider.readRSSI(this);
    }
  }

  /**
   * @description call back for reading an RSSI from the device
   * @param {number} rssi
   */
  onReadRSSI (rssi:number) {
    this.rssi = rssi;
    this.isPolling = false;
    this.deviceDiscoveredStore.dispatch({type: DeviceDiscoveredAction.DEVICE_UPDATED, payload:this});
  }

  /**
   * @description prepares the data gotten from the device for storage
   * @param {Array<any>} data
   * @returns {DeviceTimeData}
   */
  protected prepareDataForStorage (data:Array<any>):DeviceTimeData {
    let timestamp:number = data[1];
    timestamp = this.streamTimeHelper.processTimestamp(timestamp);
    //console.log('stream time information', JSON.stringify(this.streamTimeHelper));
    const dataKey:string = timestamp + ':' + this.location;
    let returnData:DeviceTimeData = {};
    this.battery = data[2];
    const seriesData:Array<string> = data[3];
    let deviceData:DeviceData = {};
    // Device details returned a list of the keys that the array data here will match to. We match it to the data from the device and make a new array.
    for (let n=0; n < this.dataToCapture.length; n++) {
      let key = this.dataToCapture[n];
      deviceData[key] = seriesData[n];
    }
    deviceData.deviceId = this.getPeripheralId();
    deviceData.location = this.location;
    deviceData.timestamp = timestamp;
    deviceData.machineLearningSessionId = this.machineLearningSessionId;
    returnData[dataKey] = deviceData;
    return returnData;
  }



  /**
   * @description parses the response from a READ request to the device. Not used on current device.
   * @param data
   */
  onRead(data?:any){
    //Not used
  }

  /**
   * @description fires when connected. Assigns notifications to this model, stores data about the peripheral and dispatches to the stores
   * @param {PeripheralInterface} data
   */
  onConnect(data:PeripheralInterface) {
    this.peripheral = data;
    this.assignNotifications();
    this.isConnected = true;
    this.deviceDiscoveredStore.dispatch({type: DeviceDiscoveredAction.DEVICE_UPDATED, payload:this});
    //TODO: put this back
    //this.sensorLogFrequency();
    //this.sensorAssignment();
    this.ddGet();
  }


  /**
   * @description fires on disconnect of device. updated the device and dispatches to the stores.
   * @param data
   */
  onDisconnect(data?:any){
    this.isConnected = false;
    this.deviceDiscoveredStore.dispatch({type: DeviceDiscoveredAction.DEVICE_DISCONNECTED, payload:this});
  }

  /**
   * @description gets the id of the peripheral assigned to the model.
   * @returns {string}
   */
  getPeripheralId():string{
    return this.peripheral.id;
  }

  onPaired() {
    this.isPaired = true;
    this.deviceDiscoveredStore.dispatch({type: DeviceDiscoveredAction.DEVICE_UPDATED, payload:this});
  }

}
