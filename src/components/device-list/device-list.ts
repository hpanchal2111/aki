import {DeviceModel} from "../../models/device.model";
import {Component, Input, NgZone} from '@angular/core';
import {Store} from "@ngrx/store";
import {DeviceDiscoveredState} from "../../reducers/devices/discovered/device-discovered.state";
import {ToastProvider} from "../../providers/toast/toast.provider";
import {HasDevicesInterface} from "../../interfaces/has-devices.interface";
import {DeviceDiscoveredStoreToViewHelper} from "../../helper/store-to-view/device-connected-store-to-view.helper";

/**
 * Generated class for the DeviceListComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'device-list',
  templateUrl: 'device-list.html'
})
export class DeviceListComponent implements HasDevicesInterface {
  deviceDiscoveredStore:Store<DeviceDiscoveredState>;
  toastProvider: ToastProvider;
  @Input() devices: Array<DeviceModel>;
  protected deviceDiscoveredToStoreHelper:DeviceDiscoveredStoreToViewHelper;
  zone:NgZone;

  text: string;

  constructor(
    deviceDiscoveredStore: Store<DeviceDiscoveredState>,
    toastProvider: ToastProvider
  ) {
    this.deviceDiscoveredStore = deviceDiscoveredStore;
    this.toastProvider = toastProvider;
    this.deviceDiscoveredToStoreHelper = new DeviceDiscoveredStoreToViewHelper(this);
  }

  ngOnInit () {
    this.deviceDiscoveredToStoreHelper.bindDeviceDiscoveredStore();
  }

}

