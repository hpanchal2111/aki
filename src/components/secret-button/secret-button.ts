import { Component, Input, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TabsPage } from '../../pages/tabs/tabs';
/**
 * Generated class for the SecretButtonComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'secret-button',
  templateUrl: 'secret-button.html'
})
export class SecretButtonComponent {

  @Input() color: any;
  @ViewChild('secretBtn') secretBtn;

  constructor(
  	public navCtrl: NavController
  	) {
  }

  redirectToDashboard()
  {
    // go to tabs page default selected Dashboard 
    if(this.navCtrl.parent)
      this.navCtrl.parent.select(2);
    else
      this.navCtrl.push(TabsPage);
  }
}
