import { Component, ViewChild, Input } from '@angular/core';
import { Chart } from 'chart.js';

/**
 * Generates a Polar Area chart
 *
 * See http://www.chartjs.org/docs/latest/charts/polar.html for dataset properties and options
 * 
 */
@Component({
  selector: 'polar-chart',
  templateUrl: 'polar-chart.html'
})
export class PolarChartComponent {

  @ViewChild('polarCanvas') polarCanvas;
  polarChart: any;

  // Dataset Properties
  @Input() data: any;

  // Configuration Options
  @Input() options: any;

  constructor() {
  }

  ngOnInit() {

    // create chart
    this.polarChart = new Chart(this.polarCanvas.nativeElement, {

      type: 'polarArea',
      data: this.data,
      options: this.options

    });

  }

}
