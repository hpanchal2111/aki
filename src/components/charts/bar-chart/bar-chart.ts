import { Component, ViewChild, Input } from '@angular/core';
import { Chart } from 'chart.js';


/**
 * Generates a bar chart
 *
 * See http://www.chartjs.org/docs/latest/charts/bar.html for dataset properties and options
 * 
 */
@Component({
  selector: 'bar-chart',
  templateUrl: 'bar-chart.html'
})
export class BarChartComponent {

  @ViewChild('barCanvas') barCanvas;
  barChart: any;

  // Dataset Properties
  @Input() data: any;

  // Configuration Options
  @Input() options: any;

  constructor() {
  }

  ngOnInit() {

    // create chart
    this.barChart = new Chart(this.barCanvas.nativeElement, {

      type: 'bar',
      data: this.data,
      options: this.options

    });

  }

}
