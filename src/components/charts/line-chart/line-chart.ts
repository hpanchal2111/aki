import { Component, ViewChild, Input } from '@angular/core';
import { Chart } from 'chart.js';

/**
 * Generates a line chart
 *
 * See http://www.chartjs.org/docs/latest/charts/line.html for dataset properties and options
 * 
 */
@Component({
  selector: 'line-chart',
  templateUrl: 'line-chart.html'
})
export class LineChartComponent {

  @ViewChild('lineCanvas') lineCanvas;
  lineChart: any;

  // Dataset Properties
  @Input() data: any;

  // Configuration Options
  @Input() options: any;

  constructor() {
  }

  ngOnInit() {

    // create chart
    this.lineChart = new Chart(this.lineCanvas.nativeElement, {

      type: 'line',
      data: this.data,
      options: this.options

    });

  }


}
