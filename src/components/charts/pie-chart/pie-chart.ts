import { Component, ViewChild, Input } from '@angular/core';
import { Chart } from 'chart.js';

/**
 * Generates a pie chart
 *
 * See http://www.chartjs.org/docs/latest/charts/doughnut.html for dataset properties and options
 * 
 */
@Component({
  selector: 'pie-chart',
  templateUrl: 'pie-chart.html'
})
export class PieChartComponent {

  @ViewChild('pieCanvas') pieCanvas;
  pieChart: any;

  // Dataset Properties
  @Input() data: any;

  // Configuration Options
  @Input() options: any;

  constructor() {
  }

  ngOnInit() {

    // create chart
    this.pieChart = new Chart(this.pieCanvas.nativeElement, {

      type: 'pie',
      data: this.data,
      options: this.options

    });

  }

}
