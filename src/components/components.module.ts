import { NgModule } from '@angular/core';
import { DeviceListComponent } from './device-list/device-list';
import { IonicModule } from 'ionic-angular';
import { SecretButtonComponent } from './secret-button/secret-button';

@NgModule({
	declarations: [DeviceListComponent,
    SecretButtonComponent],
	imports: [IonicModule],
	exports: [DeviceListComponent,
    SecretButtonComponent]
})
export class ComponentsModule {}
