# AKI Movement Laboratory Ionic App

### Requirments

The app is built using

* `NodeJS`
* `NPM`
* `Ionic`
* `Cordova`

To install Node and NPM, go to: [NodeJS](https://nodejs.org/en/)

Install Cordova: `sudo npm install -g cordova`

Install Ionic: `sudo npm install -g ionic`

### Setting up a local environment

After cloning the code base, cd into the directory and run `npm install`. This will install all the project dependencies. 

Once the dependency installation is complete run: `ionic serve`. This will start a local dev environment allowing you to run the app in your browser, and if you installed and set up the Ionic DevApp run a liveReload server on the test device.

For more info on the Ionic DevApp go to: [Ionic DevApp](https://ionicframework.com/docs/pro/devapp/)
