export namespace ENV {
  export const ML_API_URL = 'http://{server-url}:3000/';
  export const PROFILE_API_URL = 'http://{server-url}:3000/';
  export const PUBNUB_PUBLISH_KEY: string = 'key';
  export const PUBNUB_SUBSCRIBE_KEY: string = 'key';
  export const JWT_SECRET: string = 'secret';
}
